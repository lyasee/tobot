CREATE SCHEMA `tobot` DEFAULT CHARACTER SET utf8 ;

CREATE TABLE `tobot`.`tbl_bus_c1_ji` (
  `idx` INT NOT NULL AUTO_INCREMENT,
  `time_str` VARCHAR(100) NOT NULL,
  `time` TIME NOT NULL,
  PRIMARY KEY (`idx`));

CREATE TABLE `tobot`.`tbl_bus_ji_c2` (
  `idx` INT NOT NULL AUTO_INCREMENT,
  `time_str` VARCHAR(100) NOT NULL,
  `time` TIME NOT NULL,
  PRIMARY KEY (`idx`));

CREATE TABLE `tobot`.`tbl_bus_c1_c2` (
  `idx` INT NOT NULL AUTO_INCREMENT,
  `time_str` VARCHAR(100) NOT NULL,
  `time` TIME NOT NULL,
  PRIMARY KEY (`idx`));

CREATE TABLE `tobot`.`tbl_bus_c2_c1` (
  `idx` INT NOT NULL AUTO_INCREMENT,
  `time_str` VARCHAR(100) NOT NULL,
  `time` TIME NOT NULL,
  PRIMARY KEY (`idx`));

CREATE TABLE `tobot`.`tbl_bus_il_c2` (
  `idx` INT NOT NULL AUTO_INCREMENT,
  `time_str` VARCHAR(100) NOT NULL,
  `time` TIME NOT NULL,
  PRIMARY KEY (`idx`));

CREATE TABLE `tobot`.`tbl_bus_c2_il` (
  `idx` INT NOT NULL AUTO_INCREMENT,
  `time_str` VARCHAR(100) NOT NULL,
  `time` TIME NOT NULL,
  PRIMARY KEY (`idx`));

CREATE TABLE `tobot`.`tbl_bus_c1_il` (
  `idx` INT NOT NULL AUTO_INCREMENT,
  `time_str` VARCHAR(100) NOT NULL,
  `time` TIME NOT NULL,
  PRIMARY KEY (`idx`));

CREATE TABLE `tobot`.`tbl_bus_c2_ji` (
  `idx` INT NOT NULL AUTO_INCREMENT,
  `time_str` VARCHAR(100) NOT NULL,
  `time` TIME NOT NULL,
  PRIMARY KEY (`idx`));