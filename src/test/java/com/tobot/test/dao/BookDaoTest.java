package com.tobot.test.dao;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.tobot.dao.BookDao;
import com.tobot.domain.Book;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/spring/*.xml"})
public class BookDaoTest {
	
	@Autowired
	private BookDao bookDao;
	
	@Test
	public void findBookTest() {
		
		List<Book> books = new ArrayList<Book>();
		
		try {
			
			books = bookDao.findBook("앵귤러");
			
			for(Book book : books) {
				System.out.println("title: " + book.getTitle());
				System.out.println("campus: " + book.getCampus());
				System.out.println("code: " + book.getCode());
				System.out.println("stats: " + book.getState());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void bookDetailTest() {
		
		Book book = new Book();
		
		try {
			
			book = bookDao.bookDetail("282128", "도서관(제2캠퍼스)");
			
			System.out.println("barcode: " + book.getBarcode());
			System.out.println("volCopy: " + book.getVolCopy());
			System.out.println("place: " + book.getPlace());
			System.out.println("state: " + book.getState());
			System.out.println("reservebtn: " + book.getReservebtn());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	

}
