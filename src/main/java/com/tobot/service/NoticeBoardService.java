package com.tobot.service;

import java.util.List;

import com.tobot.domain.Board;
import com.tobot.domain.Criteria;
import com.tobot.domain.SearchCriteria;

public interface NoticeBoardService {
	public void insert(Board board) throws Exception;
	
	public Board read(Integer idx) throws Exception;
	
	public void modify(Board board) throws Exception;
	
	public void remove(Integer idx) throws Exception;
	
	public List<Board>  listAll() throws Exception;
	
	public List<Board>  listCriteria(Criteria cri) throws Exception;
	
	public int listCountCriteria(Criteria cri) throws Exception;
	
	public List<Board> listSearchCriteria(SearchCriteria cri)	throws Exception;
	
	public int listSearchCount(SearchCriteria cri) throws Exception;

}
