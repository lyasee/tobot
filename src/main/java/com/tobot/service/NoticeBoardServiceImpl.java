package com.tobot.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.tobot.dao.NoticeBoardDAO;
import com.tobot.domain.Board;
import com.tobot.domain.Criteria;
import com.tobot.domain.SearchCriteria;

@Service
public class NoticeBoardServiceImpl implements NoticeBoardService {
	@Inject
	private NoticeBoardDAO dao;
	
	@Override
	public void insert(Board board) throws Exception {
		dao.insert(board);
	}

	@Override
	public Board read(Integer idx) throws Exception {
		return dao.read(idx);
	}

	@Override
	public void modify(Board board) throws Exception {
		dao.modify(board);
	}

	@Override
	public void remove(Integer idx) throws Exception {
		dao.remove(idx);		
	}

	@Override
	public List<Board> listAll() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Board> listCriteria(Criteria cri) throws Exception {
		return dao.listCriteria(cri);
	}

	@Override
	public int listCountCriteria(Criteria cri) throws Exception {
		return dao.countPaging(cri);
	}

	@Override
	public List<Board> listSearchCriteria(SearchCriteria cri) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int listSearchCount(SearchCriteria cri) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

}
