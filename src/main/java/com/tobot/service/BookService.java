package com.tobot.service;

import java.util.Map;

public interface BookService {
	
	public Map<String, Object> init() throws Exception;
	
	public Map<String, Object> findBook(String name) throws Exception;

}
