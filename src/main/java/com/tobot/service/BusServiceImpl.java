package com.tobot.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tobot.dao.BusDao;
import com.tobot.domain.Bus;
import com.tobot.domain.Status;
import com.tobot.util.Compare;

@Service
public class BusServiceImpl implements BusService{
	
	@Autowired
	private KeyboardService keyboardService;
	
	@Autowired
	private TimeService timeService;
	
	@Autowired
	private BusDao busDao;
	
	@Override
	public Map<String, Object> findBus(String line) throws Exception {
		
		if(line.contains("1캠퍼스 -> 2캠퍼스")) 
			return json(busDao.getC1toC2(timeService.currentTime()));
		
		else if(line.contains("2캠퍼스 -> 1캠퍼스")) 
			return json(busDao.getC2toC1(timeService.currentTime()));
		
		else if(line.contains("2캠퍼스 -> 지행역")) 
			return json(busDao.getC2toJi(timeService.currentTime()));
		
		else if(line.contains("1캠퍼스 -> 일산")) 
			return json(busDao.getC1toIl(timeService.currentTime()));
		
		else if(line.contains("2캠퍼스 -> 일산")) 
			return json(busDao.getC2toIl(timeService.currentTime()));
		
		else if(line.contains("1캠퍼스 -> 지행")) 
			return json(busDao.getC1toJi(timeService.currentTime()));
		
		else if(line.contains("일산 -> 2캠퍼스")) 
			return json(busDao.getIltoC2(timeService.currentTime()));
		
		else if(line.contains("지행 -> 2캠퍼스")) 
			return json(busDao.getJitoC2(timeService.currentTime()));
		
		return null;
	}
	
	@Override
	public Map<String, Object> lineButton() throws Exception {
		
		Map<String, Object> json = new LinkedHashMap<String, Object>();
		Map<String, Object> json_message = new LinkedHashMap<String, Object>();
		
		List<String> buttons = new ArrayList<String>();
		
		buttons.add("1캠퍼스 -> 2캠퍼스");
		buttons.add("2캠퍼스 -> 1캠퍼스");
		buttons.add("2캠퍼스 -> 지행역");
		buttons.add("1캠퍼스 -> 일산");
		buttons.add("2캠퍼스 -> 일산");
		buttons.add("1캠퍼스 -> 지행");
		buttons.add("일산 -> 2캠퍼스");
		buttons.add("지행 -> 2캠퍼스");
		
		Status status = new Status();
		
		status.setCode(200);
		status.setName("OK");
		status.setMsg("정상적으로 처리되었습니다.");
		
		String text = "궁금한 노선을 선택해줘!";
		
		json_message.put("text", text);
		
		json.put("status", status);
		json.put("message", json_message);
		json.put("keyboard", keyboardService.button(buttons));
		
		return json;
	}
	
	/**
	 * 사용자에게 뿌려질 정보 JSON
	 * @param bus
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	public Map<String, Object> json(List<Bus> bus) throws Exception {
		
		Map<String, Object> json = new LinkedHashMap<String, Object>();
		Map<String, Object> json_message = new LinkedHashMap<String, Object>();
		
		ArrayList<Integer> min = new ArrayList<Integer>(); // 분 계산 결과
		
		String text = "";
		
		if(timeService.isWeekend()) {
			text = "주말이라 버스가 없어...";
		}else {
		
			if(bus.isEmpty()){ // 데이터가 없는 경우
				text = "현재 버스가 없어 ㅠㅠ";
			}else{
				min = timeService.changeTime(timeService.currentTime(), bus);
				text = "버스시간을 알려줄께!!\n"
						+ min.get(0) + "분 후에 있어!\n";
						
				if(min.size() > 1) {
					text = text + "다음 버스는 " + min.get(1) + "분 후에 있어!";
				}else {
					text = text + "이번이 마지막 버스야!";
				}
			}
			
		}
		
		json_message.put("text", text);
		json.put("message", json_message);
		json.put("keyboard", keyboardService.init());
		
		return json;
	}
	
	// -----------------------------------------------------------
	// |					전체 버스 시간 조회						 |
	// -----------------------------------------------------------

	@Override
	public List<Bus> getAllC1toC2() throws Exception {		
		return busDao.getAllC1toC2();
	}

	@Override
	public List<Bus> getAllC1toIl() throws Exception {
		return busDao.getAllC1toIl();
	}

	@Override
	public List<Bus> getAllC1toJi() throws Exception {
		return busDao.getAllC1toJi();
	}

	@Override
	public List<Bus> getAllC2toC1() throws Exception {
		return busDao.getAllC2toC1();
	}

	@Override
	public List<Bus> getAllC2toJi() throws Exception {
		return busDao.getAllC2toJi();
	}

	@Override
	public List<Bus> getAllC2toIl() throws Exception {
		return busDao.getAllC2toIl();
	}

	@Override
	public List<Bus> getAllIltoC2() throws Exception {
		return busDao.getAllIltoC2();
	}

	@Override
	public List<Bus> getAllJitoC2() throws Exception {
		return busDao.getAllJitoC2();
	}
	
	// -----------------------------------------------------------
	// |						해당 시간에 버스 조회					 |
	// -----------------------------------------------------------

	@Override
	public List<Bus> getC1toC2() throws Exception {
		return busDao.getC1toC2(timeService.currentTime());
	}

	@Override
	public List<Bus> getC1toIl() throws Exception {
		return busDao.getC1toIl(timeService.currentTime());
	}

	@Override
	public List<Bus> getC1toJi() throws Exception {
		return busDao.getC1toJi(timeService.currentTime());
	}

	@Override
	public List<Bus> getC2toC1() throws Exception {
		return busDao.getC2toC1(timeService.currentTime());
	}

	@Override
	public List<Bus> getC2toJi() throws Exception {
		return busDao.getC2toJi(timeService.currentTime());
	}

	@Override
	public List<Bus> getC2toIl() throws Exception {
		return busDao.getC2toIl(timeService.currentTime());
	}

	@Override
	public List<Bus> getIltoC2() throws Exception {
		return busDao.getIltoC2(timeService.currentTime());
	}

	@Override
	public List<Bus> getJitoC2() throws Exception {
		return busDao.getJitoC2(timeService.currentTime());
	}
	
	/**
	 * JSON 형식에 맞게 반환
	 * @param bus
	 * @return
	 */
	public Map<String, Object> apiJson(List<Bus> bus) {
		
		Map<String, Object> json = new LinkedHashMap<String, Object>();
		
		Status status = new Status();
		
		if(bus.isEmpty()){ // null 은 객체주소가 같은지 판단, isEmpty() 값이 같은지 판단
			
//			status.setCode(404);
//			status.setName("Not Found");
//			status.setMsg("데이터를 찾을 수 없습니다.");
//			
//			json.put("status", status);
//			json.put("data", null);
			
			return null;
			
		}
		
		status.setCode(200);
		status.setName("OK");
		status.setMsg("정상적으로 처리되었습니다.");
		
		json.put("status", status);
		json.put("data", bus);
		
		return json;
	}
	
	// -----------------------------------------------------------
	// |						시간표 테이블 삽입	 				 |
	// -----------------------------------------------------------

	@Override
	public void insertC2toji(Bus vo) throws Exception {
		busDao.insertC2toJi(vo);
	}

	@Override
	public void insertjitoC2(Bus vo) throws Exception {
		busDao.insertJitoC2(vo);
	}

	@Override
	public void insertC1toC2(Bus vo) throws Exception {
		busDao.insertC1toC2(vo);
	}

	@Override
	public void insertC2toC1(Bus vo) throws Exception {
		busDao.insertC2toC1(vo);
	}

	@Override
	public void insertC2toIl(Bus vo) throws Exception {
		busDao.insertC2toIl(vo);
	}

	@Override
	public void insertIltoC2(Bus vo) throws Exception {
		busDao.insertIltoC2(vo);
	}

	@Override
	public void insertC1toIl(Bus vo) throws Exception {
		busDao.insertC1toIl(vo);
	}

	// -----------------------------------------------------------
	// |						시간표 테이블 수정	 				 |
	// -----------------------------------------------------------

	@Override
	public void updateC2toji(Bus vo) throws Exception {
		busDao.updateC2toJi(vo);		
	}


	@Override
	public void updateJitoC2(Bus vo) throws Exception {
		busDao.updateJitoC2(vo);
	}

	@Override
	public void updateC1toC2(Bus vo) throws Exception {
		busDao.updateC1toC2(vo);
	}

	@Override
	public void updateC2toC1(Bus vo) throws Exception {
		busDao.updateC2toC1(vo);
	}

	@Override
	public void updateC2toIl(Bus vo) throws Exception {
		busDao.updateC2toIl(vo);
	}

	@Override
	public void updateIltoC2(Bus vo) throws Exception {
		busDao.updateIltoC2(vo);
	}

	@Override
	public void updateC1toIl(Bus vo) throws Exception {
		busDao.updateC1toIl(vo);
	}

	// -----------------------------------------------------------
	// |						시간표 테이블 삭제	 				 |
	// -----------------------------------------------------------
	@Override
	public void deleteC2toji(int idx) throws Exception {
		busDao.deleteC2toJi(idx);		
	}


	@Override
	public void deleteJitoC2(int idx) throws Exception {
		busDao.deleteJitoC2(idx);
	}

	@Override
	public void deleteC1toC2(int idx) throws Exception {
		busDao.deleteC1toC2(idx);
	}

	@Override
	public void deleteC2toC1(int idx) throws Exception {
		busDao.deleteC2toC1(idx);
	}

	@Override
	public void deleteC2toIl(int idx) throws Exception {
		busDao.deleteC2toIl(idx);
	}

	@Override
	public void deleteIltoC2(int idx) throws Exception {
		busDao.deleteIltoC2(idx);
	}

	@Override
	public void deleteC1toIl(int idx) throws Exception {
		busDao.deleteC1toIl(idx);
	}


	// -----------------------------------------------------------
	// |					시간표 정렬            						 |
	// -----------------------------------------------------------
	@Override
	public void sortTable(String busTable) throws Exception {
		ArrayList<Bus> list = null;
		
		if(busTable == "c2toji"){
			list = (ArrayList<Bus>) busDao.getAllC2toJi();
			busDao.deleteAllC2toJi();
			
			Collections.sort(list, new Compare<Bus>());
			
			for (int i = 0; i < list.size(); i++) {
				Bus insertBus = list.get(i);
				insertBus.setIdx(i+1);
				busDao.insertC2toJi(insertBus);
			}
			
		} else if(busTable == "jitoc2"){
			list = (ArrayList<Bus>) busDao.getAllJitoC2();
			busDao.deleteAllJitoC2();;
			
			Collections.sort(list, new Compare<Bus>());
			
			for (int i = 0; i < list.size(); i++) {
				Bus insertBus = list.get(i);
				insertBus.setIdx(i+1);
				busDao.insertJitoC2(insertBus);
			}
			
		} else if(busTable == "c1toc2"){
			list = (ArrayList<Bus>) busDao.getAllC1toC2();
			busDao.deleteAllC1toC2();
			
			Collections.sort(list, new Compare<Bus>());
			
			for (int i = 0; i < list.size(); i++) {
				Bus insertBus = list.get(i);
				insertBus.setIdx(i+1);
				busDao.insertC1toC2(insertBus);
			}
			
		} else if(busTable == "c2toc1"){
			list = (ArrayList<Bus>) busDao.getAllC2toC1();
			busDao.deleteAllC2toC1();
			
			Collections.sort(list, new Compare<Bus>());
			
			for (int i = 0; i < list.size(); i++) {
				Bus insertBus = list.get(i);
				insertBus.setIdx(i+1);
				busDao.insertC2toC1(insertBus);
			}
			
		} else if(busTable == "iltoc2"){
			list = (ArrayList<Bus>) busDao.getAllIltoC2();
			busDao.deleteAllIltoC2();
			
			Collections.sort(list, new Compare<Bus>());
			
			for (int i = 0; i < list.size(); i++) {
				Bus insertBus = list.get(i);
				insertBus.setIdx(i+1);
				busDao.insertIltoC2(insertBus);
			}
			
		} else if(busTable == "c2toil"){
			list = (ArrayList<Bus>) busDao.getAllC2toIl();
			busDao.deleteAllC2toIl();
			
			Collections.sort(list, new Compare<Bus>());
			
			for (int i = 0; i < list.size(); i++) {
				Bus insertBus = list.get(i);
				insertBus.setIdx(i+1);
				busDao.insertC2toIl(insertBus);
			}
			
		}
		

		
	}
}
