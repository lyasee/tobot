package com.tobot.service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tobot.dao.BookDao;
import com.tobot.domain.Book;
import com.tobot.domain.Status;

@Service
public class BookServiceImpl implements BookService {
	
	@Autowired
	private KeyboardService keyboardService;
	
	@Autowired
	private BookDao bookDao;

	@Override
	public Map<String, Object> init() throws Exception {
		
		Map<String, Object> json = new LinkedHashMap<String, Object>();
		Map<String, Object> json_message = new LinkedHashMap<String, Object>();
			
		Status status = new Status();
		
		status.setCode(200);
		status.setName("OK");
		status.setMsg("정상적으로 처리되었습니다.");
		
		String text = "제목을 입력해줘~";
		
		json_message.put("text", text);
		
		json.put("status", status);
		json.put("message", json_message);
		json.put("keyboard", keyboardService.text());
		
		return json;
	}

	@Override
	public Map<String, Object> findBook(String name) throws Exception {
		
		Map<String, Object> json = new LinkedHashMap<String, Object>();
		Map<String, Object> json_message = new LinkedHashMap<String, Object>();
		
		List<Book> books = bookDao.findBook(name);
		
		if(books.isEmpty()) {
			return null;
		}
		
		String text = "검색 결과: " + books.size() + "건\n\n";
		for(Book book : books) {
			text = text + "[제목]: " + book.getTitle() + "\n";
			text = text + "[장소]: " + book.getCampus() + "\n";
			text = text + "[위치]: " + book.getCollno() + "\n";		
//			text = text + "[대출]: " + book.getState() + "\n\n";
			
		}
		
		json_message.put("text", text);
		json.put("message", json_message);
		json.put("keyboard", keyboardService.init());
		
		return json;
	}

		
}
