package com.tobot.service;

import java.util.List;
import java.util.Map;

import com.tobot.domain.Bus;

public interface BusService {
	
	/**
	 * 버스 노선명으로 데이터 찾기
	 * @param line
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	public Map<String, Object> findBus(String line) throws Exception;
	
	/**
	 * 버스 라인을 버튼으로 반환
	 * @return Map<String, Object>
	 * @throws Exception
	 */
	public Map<String, Object> lineButton() throws Exception;
	
	/**
	 * 1캠퍼스 -> 2캠퍼스, 전체 데이터
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getAllC1toC2() throws Exception;
	
	/**
	 * 1캠퍼스 -> 일산, 전체 데이터
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getAllC1toIl() throws Exception;
	
	/**
	 * 1캠퍼스 -> 지행, 전체 데이터
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getAllC1toJi() throws Exception;
	
	/**
	 * 2캠퍼스 -> 1캠퍼스, 전체 데이터
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getAllC2toC1() throws Exception;
	
	/**
	 * 2캠퍼스 -> 지행, 전체 데이터
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getAllC2toJi() throws Exception;
	
	/**
	 * 2캠퍼스 -> 일산, 전체 데이터
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getAllC2toIl() throws Exception;
	
	/**
	 * 일산 -> 2캠퍼스, 전체 데이터
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getAllIltoC2() throws Exception;
	
	/**
	 * 지행 -> 2캠퍼스, 전체 데이터
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getAllJitoC2() throws Exception;
	
	// ---------------------------------------------------
	// 현재 시간에 맞는 버스 2개 반환
	// ---------------------------------------------------
	/**
	 * 1캠퍼스 -> 2캠퍼스, 현재 시간에 맞는 2개
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getC1toC2() throws Exception;
	
	/**
	 * 1캠퍼스 -> 일산, 현재 시간에 맞는 2개
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getC1toIl() throws Exception;
	
	/**
	 * 1캠퍼스 -> 지행, 현재 시간에 맞는 2개
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getC1toJi() throws Exception;
	
	/**
	 * 2캠퍼스 -> 1캠퍼스, 현재 시간에 맞는 2개
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getC2toC1() throws Exception;
	
	/**
	 * 2캠퍼스 -> 지행, 현재 시간에 맞는 2개
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getC2toJi() throws Exception;
	
	/**
	 * 2캠퍼스 -> 일산, 현재 시간에 맞는 2개
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getC2toIl() throws Exception;
	
	/**
	 * 일산 -> 2캠퍼스, 현재 시간에 맞는 2개
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getIltoC2() throws Exception;
	
	/**
	 * 지행 -> 2캠퍼스, 현재 시간에 맞는 2개
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getJitoC2() throws Exception;

	// ---------------------------------------------------
	// 시간표 테이블 삽입
	// ---------------------------------------------------
	/**
	 * 2캠퍼스 -> 지행 시간표 삽입
	 * @return void
	 * @param Bus
	 * @throws Exception
	 */
	public void insertC2toji(Bus vo) throws Exception;
	/**
	 * 지행  -> 2캠퍼스 시간표 삽입
	 * @return void
	 * @param Bus
	 * @throws Exception
	 */
	public void insertjitoC2(Bus vo) throws Exception;
	/**
	 * 1캠퍼스 -> 2캠퍼스 시간표 삽입
	 * @return void
	 * @param Bus
	 * @throws Exception
	 */
	public void insertC1toC2(Bus vo) throws Exception;
	/**
	 * 2캠퍼스 -> 1캠퍼스 시간표 삽입
	 * @return void
	 * @param Bus
	 * @throws Exception
	 */
	public void insertC2toC1(Bus vo) throws Exception;
	/**
	 * 2캠퍼스 -> 일산 시간표 삽입
	 * @return void
	 * @param Bus
	 * @throws Exception
	 */
	public void insertC2toIl(Bus vo) throws Exception;
	/**
	 * 일산 -> 2캠퍼스 시간표 삽입
	 * @return void
	 * @param Bus
	 * @throws Exception
	 */
	public void insertIltoC2(Bus vo) throws Exception;
	/**
	 * 1캠퍼스 -> 일산 시간표 삽입
	 * @return void
	 * @param Bus
	 * @throws Exception
	 */
	public void insertC1toIl(Bus vo) throws Exception;
	
	// ---------------------------------------------------
	// 시간표 테이블 수정
	// ---------------------------------------------------
	/**
	 * 2캠퍼스 -> 지행 시간표 수정
	 * @return void
	 * @param Bus
	 * @throws Exception
	 */
	public void updateC2toji(Bus vo) throws Exception;
	/**
	 * 2캠퍼스 -> 지행 시간표 수정
	 * @return void
	 * @param Bus
	 * @throws Exception
	 */
	public void updateJitoC2(Bus vo) throws Exception;
	/**
	 * 2캠퍼스 -> 지행 시간표 수정
	 * @return void
	 * @param Bus
	 * @throws Exception
	 */
	public void updateC1toC2(Bus vo) throws Exception;
	/**
	 * 2캠퍼스 -> 지행 시간표 수정
	 * @return void
	 * @param Bus
	 * @throws Exception
	 */
	public void updateC2toC1(Bus vo) throws Exception;
	/**
	 * 2캠퍼스 -> 지행 시간표 수정
	 * @return void
	 * @param Bus
	 * @throws Exception
	 */
	public void updateC2toIl(Bus vo) throws Exception;
	/**
	 * 2캠퍼스 -> 지행 시간표 수정
	 * @return void
	 * @param Bus
	 * @throws Exception
	 */
	public void updateIltoC2(Bus vo) throws Exception;
	/**
	 * 2캠퍼스 -> 지행 시간표 수정
	 * @return void
	 * @param Bus
	 * @throws Exception
	 */
	public void updateC1toIl(Bus vo) throws Exception;

	// ---------------------------------------------------
	// 시간표 테이블 삭제
	// ---------------------------------------------------
	/**
	 * 2캠퍼스 -> 지행 시간표 삭제
	 * @param int
	 * @throws Exception
	 */
	public void deleteC2toji(int idx) throws Exception;
	/**
	 * 2캠퍼스 -> 지행 시간표 삭제
	 * @param int
	 * @throws Exception
	 */
	public void deleteJitoC2(int idx) throws Exception;
	/**
	 * 2캠퍼스 -> 지행 시간표 삭제
	 * @param int
	 * @throws Exception
	 */
	public void deleteC1toC2(int idx) throws Exception;
	/**
	 * 2캠퍼스 -> 지행 시간표 삭제
	 * @param int
	 * @throws Exception
	 */
	public void deleteC2toC1(int idx) throws Exception;
	/**
	 * 2캠퍼스 -> 지행 시간표 삭제
	 * @param int
	 * @throws Exception
	 */
	public void deleteC2toIl(int idx) throws Exception;
	/**
	 * 2캠퍼스 -> 지행 시간표 삭제
	 * @param int
	 * @throws Exception
	 */
	public void deleteIltoC2(int idx) throws Exception;
	/**
	 * 2캠퍼스 -> 지행 시간표 삭제
	 * @param int
	 * @throws Exception
	 */
	public void deleteC1toIl(int idx) throws Exception;

	// ---------------------------------------------------
	// 시간표 정렬
	// ---------------------------------------------------
	/**
	 * 2캠퍼스 -> 지행 시간표 삭제
	 * @param int
	 * @throws Exception
	 */
	public void sortTable(String string) throws Exception;
}
