package com.tobot.service;

import java.util.List;

import com.tobot.domain.Keyboard;

public interface KeyboardService {
	
	public Keyboard init() throws Exception;
	
	public Keyboard button(List<String> buttons) throws Exception;
	
	public Keyboard text() throws Exception;

}
