package com.tobot.service;

import com.tobot.domain.User;

public interface UserService {
	public User login(User vo) throws Exception;
}