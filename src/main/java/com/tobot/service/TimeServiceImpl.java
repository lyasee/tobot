package com.tobot.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.stereotype.Service;

import com.tobot.domain.Bus;

@Service
public class TimeServiceImpl implements TimeService{

	@Override
	public String currentTime() throws Exception {
		
		String timeFormat = "HH:mm"; // 24 hour
		String time = new SimpleDateFormat(timeFormat).format(System.currentTimeMillis());	
		
		return time;
	}

	@Override
	public ArrayList<Integer> changeTime(String currentTime, List<Bus> bus) throws Exception {
		
		int currentMin = 0; // 현재 시간을 분으로 저장할 변수
		ArrayList<Integer> busMin = new ArrayList<Integer>(); // 디비에서 조회한 버스의 시간을 분으로 저장
		ArrayList<Integer> min = new ArrayList<Integer>(); // 분 계산 결과
		
		if(bus.isEmpty()) {
			busMin.add(0);
			busMin.add(0);
			return busMin;
		}
		
		String[] currentTimeSplit = currentTime.split(":");
		String[] busTimeSplit1 = bus.get(0).getTime_str().split(":");
		String[] busTimeSplit2 = null;
		
		// 마지막 버스가 아님
		if(bus.size() > 1) {
			busTimeSplit2 = bus.get(1).getTime_str().split(":");
		}
		
		// 현재 시간을 분으로 바꾸기
		currentMin = (Integer.parseInt(currentTimeSplit[0]) * 60) + Integer.parseInt(currentTimeSplit[currentTimeSplit.length-1]);
		busMin.add((Integer.parseInt(busTimeSplit1[0]) * 60) + Integer.parseInt(busTimeSplit1[busTimeSplit1.length-1]));
		
		// 마지막 버스시간이 아니면 다음 버스 시간도 구함
		if(busTimeSplit2 != null) {
			busMin.add((Integer.parseInt(busTimeSplit2[0]) * 60) + Integer.parseInt(busTimeSplit2[busTimeSplit2.length-1]));
		}
		
		// 남은 버스시간 구하기
		if(currentMin <= busMin.get(0)) {
			min.add(busMin.get(0) - currentMin);
			
			if(busTimeSplit2 != null) {
				min.add(busMin.get(1) - currentMin);
			}
			
		}else {
			min.add(0);
			min.add(0);
		}
		
		return min;
	}

	@Override
	public boolean isWeekend() throws Exception {
		
		Calendar calender = Calendar.getInstance();
		int num = calender.get(Calendar.DAY_OF_WEEK);
		
		if(num == 1 || num == 7) { // 1 일요일, 7 토요일
			return true;
		}
		
		return false;
	}

}
