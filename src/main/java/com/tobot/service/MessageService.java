package com.tobot.service;

import java.util.Map;

import com.tobot.domain.Message;

public interface MessageService {
	
	public Map<String, Object> answer(Message message) throws Exception;

}
