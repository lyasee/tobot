package com.tobot.service;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.tobot.dao.UserDAO;
import com.tobot.domain.User;

@Service
public class UserServiceImpl implements UserService {

	@Inject
	private UserDAO dao;
	
	@Override
	public User login(User vo) throws Exception {
		return dao.login(vo);
	}

}
