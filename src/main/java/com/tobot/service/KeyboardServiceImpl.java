package com.tobot.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.tobot.domain.Keyboard;

@Service
public class KeyboardServiceImpl implements KeyboardService{

	@Override
	public Keyboard init() throws Exception {
		
		Keyboard keyboard = new Keyboard();
		
		List<String> button = new ArrayList<String>() ;
		button.add("버스 시간이 궁금해!");
		button.add("책을 찾아줘~!");
		
		keyboard.setType("buttons");
		keyboard.setButtons(button);
		
		return keyboard;
	}

	@Override
	public Keyboard button(List<String> buttons) throws Exception {
		
		Keyboard keyboard = new Keyboard();
	
		keyboard.setType("buttons");
		keyboard.setButtons(buttons);
		
		return keyboard;
	}

	@Override
	public Keyboard text() throws Exception {
		
		Keyboard keyboard = new Keyboard();
		
		keyboard.setType("text");
		
		return keyboard;
	}

}
