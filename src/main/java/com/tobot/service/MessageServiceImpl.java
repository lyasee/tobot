package com.tobot.service;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tobot.domain.Message;

@Service
public class MessageServiceImpl implements MessageService {
	
	@Autowired
	private KeyboardService keyboardService;
	
	@Autowired
	private BusService busService;
	
	@Autowired
	private BookService bookService;

	@Override
	public Map<String, Object> answer(Message message) throws Exception {
		
		Map<String, Object> json = new LinkedHashMap<String, Object>();
		Map<String, Object> json_message = new LinkedHashMap<String, Object>();
		
		System.out.println(message.getContent());
		
		if(message.getContent().contains("버스 시간이 궁금해!")) {
			return busService.lineButton();
		}
	
		else if(message.getContent().contains("1캠퍼스 -> 2캠퍼스")) {		
			return busService.findBus(message.getContent());		
		}
		
		else if(message.getContent().contains("2캠퍼스 -> 1캠퍼스")) {	
			return busService.findBus(message.getContent());		
		}
		
		else if(message.getContent().contains("2캠퍼스 -> 지행역")) {		
			return busService.findBus(message.getContent());		
		}
		
		else if(message.getContent().contains("1캠퍼스 -> 일산")) {		
			return busService.findBus(message.getContent());		
		}
		
		else if(message.getContent().contains("2캠퍼스 -> 일산")) {		
			return busService.findBus(message.getContent());		
		}
		
		else if(message.getContent().contains("1캠퍼스 -> 지행")) {		
			return busService.findBus(message.getContent());		
		}
		
		else if(message.getContent().contains("일산 -> 2캠퍼스")) {		
			return busService.findBus(message.getContent());		
		}
		
		else if(message.getContent().contains("지행 -> 2캠퍼스")) {		
			return busService.findBus(message.getContent());		
		}
		
		if(message.getContent().contains("책을 찾아줘~!")) {
			return bookService.init();
		}
		
		else{
			
//			String text = "무슨말인지 모르겠어...";
//			
//			json_message.put("text", text);
//			json.put("message", json_message);
//			json.put("keyboard", keyboardService.init());
			
			
			
		}
		
		return bookService.findBook(message.getContent());
		
	}

	
}
