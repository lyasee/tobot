package com.tobot.service;

import java.util.ArrayList;
import java.util.List;

import com.tobot.domain.Bus;

public interface TimeService {

	/**
	 * 시간 변환, 시간:분, 12:00
	 * @return "12:00"
	 */
	public String currentTime() throws Exception;
	
	/**
	 * 남은 버스 시간 구하기
	 * @param currentTime 현재시간 10:12
	 * @param bus 버스시간 10:55
	 * @return ArrayList<Integer>
	 */
	public ArrayList<Integer> changeTime(String currentTime, List<Bus> bus) throws Exception;
	
	/**
	 * 주말인지 판단
	 * @return boolean
	 * @throws Exception
	 */
	public boolean isWeekend() throws Exception;
	
}
