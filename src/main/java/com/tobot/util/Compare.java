package com.tobot.util;

import java.util.Comparator;

import com.tobot.domain.Bus;

public class Compare<T> implements Comparator<T> {
	
	@Override
	public int compare(T o1, T o2) {
		int result = 0;
		if (o1 instanceof Bus) {
			Bus campusAndTrainDTO = (Bus) o1;
			Bus campusAndTrainDTO2 = (Bus) o2;
			result = campusAndTrainDTO.getTime_str().compareTo(campusAndTrainDTO2.getTime_str());
		}
		
		return result;
	}

}