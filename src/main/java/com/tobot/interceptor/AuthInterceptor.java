package com.tobot.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class AuthInterceptor extends HandlerInterceptorAdapter {
	private static final Logger logger = LoggerFactory.getLogger(LoginInterceptor.class);
	
	@Override
	public boolean preHandle(HttpServletRequest request,HttpServletResponse response, Object handler) throws Exception{
		HttpSession session = request.getSession();
		
		String reqUrl = request.getRequestURL().toString();
		
		// 임시로 인터셉터 특정 URL만 풀어둠
		if(reqUrl.equals("keyboard")){
			return true;
		}
		
		if(session.getAttribute("login") == null) {
			logger.info("current user is not logined");
			saveDest(request);
			
			response.sendRedirect("/login");
			return false;
			
		}
		return true;
	}
	
	private void saveDest(HttpServletRequest req) {
		String uri = req.getRequestURI();
		String query = req.getQueryString();
		
		if(query == null || query.equals("null")){
			query = "";
			
		} else {
			query = "?" + query ;
		}
		
		if(req.getMethod().equals("GET")) {
			logger.info("dest : " + (uri+query));
			req.getSession().setAttribute("dest", uri+query);
		}
				
	}
}
