package com.tobot.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api(){
        return new Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors.any())
            .paths(PathSelectors.regex("/api/.*")) // sawgger에 보이게할 경로패턴
            .build()
            .apiInfo(apiInfo());
    }

    // swagger ui 페이지에 노출할 내용들 커스텀
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
            .title("또봇 알리미")
            .description("카카오 플랫폼을 이용한 알리미")
            .version("v0.1")
            .termsOfServiceUrl("https://suki.kr")
            .license("LICENSE")
            .licenseUrl("https://suki.kr")
            .build();
    }

}
