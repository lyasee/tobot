package com.tobot.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class IndexController {
	
	private static final Logger logger = LoggerFactory.getLogger(IndexController.class);
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> getAll() {
		
		Map<String, Object> json = new LinkedHashMap<String, Object>();
		Map<String, Object> status = new LinkedHashMap<String, Object>();
		
		status.put("code", "200");
		status.put("msg", "메인페이지");
		
		json.put("status", status);
		json.put("data", "");
		
		return new ResponseEntity<Map<String,Object>>(json, HttpStatus.OK);
	}
	

}
