package com.tobot.controller;

import java.util.List;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tobot.domain.Bus;
import com.tobot.service.BusService;

@RequestMapping("/board/bus")
@Controller
public class BoardC2C1Controller {
	
	@Inject
	private BusService service;
	
	//-------------  C2 -> C1 게시판 맵핑 -------------
	
	//게시판 목록
	@RequestMapping(value = "/c2c1/c2c1List", method = RequestMethod.GET)
	public void c2tojiList(Model model) throws Exception {
		
		List<Bus> bus = service.getAllC2toC1();
		
		model.addAttribute("busList", bus);
	}
	
	//시간표 삽입 
	@RequestMapping(value = "/c2c1/c2c1Insert", method = RequestMethod.GET)
	public void c2tojiInsert(Model model) throws Exception {
		
	}
	
	//시간표 수정
	@RequestMapping(value = "/c2c1/c2c1Modify", method = RequestMethod.GET)
	public void c2tojiModifyList(Model model) throws Exception {

		List<Bus> bus = service.getAllC2toC1();
		
		model.addAttribute("busList", bus);
	}
	
	//시간표 삽입 요청
	@RequestMapping(value = "/c2c1/c2c1Insert", method = RequestMethod.POST)
	public String c2tojiInsert(Bus vo, RedirectAttributes rttr) throws Exception {
		System.out.println("vo ="+vo);
		
		service.insertC2toC1(vo);
		service.sortTable("c2toc1");

	    rttr.addFlashAttribute("code", "101"); //삽입성공 메세지

	    return "redirect:/board/bus/c2c1/c2c1List";
	}
	
	//시간표 수정 요청 AJAX
	@RequestMapping(value = "/c2c1/c2c1Modify", method = RequestMethod.POST)
	public ResponseEntity<String> c2tojiModify(Bus vo, Model model) throws Exception {

		ResponseEntity<String> entity = null;
		
		try {
			service.updateC2toC1(vo);
			service.sortTable("c2toc1");
			entity = new ResponseEntity<>("SUCCESS", HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			entity = new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return entity;
	}
	
	//시간표 삭제 요청 AJAX
	@RequestMapping(value = "/c2c1/c2c1Delete", method = RequestMethod.POST)
	public ResponseEntity<String> c2tojiDelete(@RequestParam("idx") int idx, Model model) throws Exception {
		
		ResponseEntity<String> entity = null;
		
		try {
			service.deleteC2toC1(idx);
			service.sortTable("c2toc1");
			entity = new ResponseEntity<>("SUCCESS", HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			entity = new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return entity;
	}
}
