package com.tobot.controller;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tobot.domain.User;
import com.tobot.service.UserService;

@Controller
@RequestMapping("/")
public class UserController {

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@Inject
	private UserService service;
	
	// 메인 페이지 GET
	@RequestMapping(value="/main", method=RequestMethod.GET)
	public String main() {
		return "main";
	}
	
	// 로그인 페이지 GET
	@RequestMapping(value="/login", method=RequestMethod.GET)
	public String login() {
		return "user/login";
	}
	
	// 로그인 요청 POST
	@RequestMapping(value="/loginPost", method= {RequestMethod.POST})
	public String loginPost(User vo, HttpSession session, Model model) throws Exception {
		
		User result = service.login(vo);
		
		if(result == null) {
			return "user/login";
		}

		logger.info("로그인 성공 : "+result.getNickname());
		model.addAttribute("login", result);
		
		return "main";
	}
	
	// 로그아웃 
	@RequestMapping(value="/logout", method= {RequestMethod.GET})
	public String logout(HttpSession session) throws Exception {
		session.removeAttribute("login");
		return "user/login";
	}
}
