package com.tobot.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tobot.domain.Message;

@RestController
public class MessageController {
	
	private static final Logger logger = LoggerFactory.getLogger(MessageController.class);
	
	@Autowired
	private com.tobot.service.MessageService messageService;
	
	// 사용자가 누른 버튼에 대한 응답 처리
	@RequestMapping(value = "/message", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> message(@RequestBody Message message) throws Exception {
		
		logger.info("/message 접속!!!");
		
		Map<String, Object> json = messageService.answer(message);
		
		return new ResponseEntity<Map<String, Object>>(json, HttpStatus.OK);
	}


}
