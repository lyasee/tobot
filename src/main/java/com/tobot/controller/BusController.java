package com.tobot.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tobot.domain.Bus;
import com.tobot.service.BusService;

import io.swagger.annotations.ApiOperation;

// httpStatus
// https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/http/HttpStatus.html

//@RestController
//@RequestMapping(value = "/api")
@Controller
public class BusController {
	
	private static final Logger logger = LoggerFactory.getLogger(BusController.class);
	
	@Autowired
	private BusService busService;
	
	// -----------------------------------------------------------
	// |					전체 버스 시간 조회							|
	// -----------------------------------------------------------
	@ApiOperation(response=Bus.class, value = "1캠퍼스 -> 2캠퍼스 버스 전체조회")
	@RequestMapping(value = "/api/bus/c1toc2All", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<Bus>> c1toc2All() throws Exception {
		
		List<Bus> bus = busService.getAllC1toC2();
		
		if(bus.isEmpty()) {
			return new ResponseEntity<List<Bus>>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<List<Bus>>(bus, HttpStatus.OK);
	}
	
	@ApiOperation(response=Bus.class, value = "1캠퍼스 -> 일산 버스 전체조회")
	@RequestMapping(value = "/api/bus/c1toilAll", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<Bus>> c1toilAll() throws Exception {
		
		List<Bus> bus = busService.getAllC1toIl();
		
		if(bus.isEmpty()) {
			return new ResponseEntity<List<Bus>>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<List<Bus>>(bus, HttpStatus.OK);
	}
	
	@ApiOperation(response=Bus.class, value = "1캠퍼스 -> 지행 버스 전체조회")
	@RequestMapping(value = "/api/bus/c1tojiAll", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<Bus>> c1tojiAll() throws Exception {
		
		List<Bus> bus = busService.getAllC1toJi();
		
		if(bus.isEmpty()) {
			return new ResponseEntity<List<Bus>>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<List<Bus>>(bus, HttpStatus.OK);
	}
	
	@ApiOperation(response=Bus.class, value = "2캠퍼스 -> 1캠퍼스 버스 전체조회")
	@RequestMapping(value = "/api/bus/c2toc1All", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<Bus>> c2toc1All() throws Exception {
		
		List<Bus> bus = busService.getAllC2toC1();
		
		if(bus.isEmpty()) {
			return new ResponseEntity<List<Bus>>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<List<Bus>>(bus, HttpStatus.OK);
	}
	
	@ApiOperation(response=Bus.class, value = "2캠퍼스 -> 지행 버스 전체조회")
	@RequestMapping(value = "/api/bus/c2tojiAll", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<Bus>> c2tojiAll() throws Exception {
		
		List<Bus> bus = busService.getAllC2toJi();
		
		if(bus.isEmpty()) {
			return new ResponseEntity<List<Bus>>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<List<Bus>>(bus, HttpStatus.OK);
	}
	
	@ApiOperation(response=Bus.class, value = "2캠퍼스 -> 일산 버스 전체조회")
	@RequestMapping(value = "/api/bus/c2toilAll", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<Bus>> c2toilAll() throws Exception {
		
		List<Bus> bus = busService.getAllC2toIl();
		
		if(bus.isEmpty()) {
			return new ResponseEntity<List<Bus>>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<List<Bus>>(bus, HttpStatus.OK);
	}
	
	@ApiOperation(response=Bus.class, value = "일산 -> 2캠퍼스 버스 전체조회")
	@RequestMapping(value = "/api/bus/iltoc2All", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<Bus>> iltoc2All() throws Exception {
		
		List<Bus> bus = busService.getAllIltoC2();
		
		if(bus.isEmpty()) {
			return new ResponseEntity<List<Bus>>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<List<Bus>>(bus, HttpStatus.OK);
	}
	
	@ApiOperation(response=Bus.class, value = "지행 -> 2캠퍼스 버스 전체조회")
	@RequestMapping(value = "/api/bus/jitoc2All", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<Bus>> jitoc2All() throws Exception {
		
		List<Bus> bus = busService.getAllJitoC2();
		
		if(bus.isEmpty()) {
			return new ResponseEntity<List<Bus>>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<List<Bus>>(bus, HttpStatus.OK);
	}
	
	// -----------------------------------------------------------
	// |						해당 시간에 버스 조회						|
	// -----------------------------------------------------------
	@ApiOperation(response=Bus.class, value = "1캠퍼스 -> 2캠퍼스 버스 조회")
	@RequestMapping(value = "/api/bus/c1toc2", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<Bus>> c1toc2() throws Exception {
		
		List<Bus> bus = busService.getC1toC2();
		
		if(bus.isEmpty()) {
			return new ResponseEntity<List<Bus>>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<List<Bus>>(bus, HttpStatus.OK);
	}
	
	@ApiOperation(response=Bus.class, value = "1캠퍼스 -> 일산 버스 조회")
	@RequestMapping(value = "/api/bus/c1toil", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<Bus>> c1toil() throws Exception {
		
		List<Bus> bus = busService.getC1toIl();
		
		if(bus.isEmpty()) {
			return new ResponseEntity<List<Bus>>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<List<Bus>>(bus, HttpStatus.OK);
	}
	
	@ApiOperation(response=Bus.class, value = "1캠퍼스 -> 지행 버스 조회")
	@RequestMapping(value = "/api/bus/c1toji", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<Bus>> c1toji() throws Exception {
		
		List<Bus> bus = busService.getC1toJi();
		
		if(bus.isEmpty()) {
			return new ResponseEntity<List<Bus>>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<List<Bus>>(bus, HttpStatus.OK);
	}
	
	@ApiOperation(response=Bus.class, value = "2캠퍼스 -> 1캠퍼스 버스 조회")
	@RequestMapping(value = "/api/bus/c2toc1", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<Bus>> c2toc1() throws Exception {
		
		List<Bus> bus = busService.getC2toC1();
		
		if(bus.isEmpty()) {
			return new ResponseEntity<List<Bus>>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<List<Bus>>(bus, HttpStatus.OK);
	}
	
	@ApiOperation(response=Bus.class, value = "2캠퍼스 -> 지행 버스 조회")
	@RequestMapping(value = "/api/bus/c2toji", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<Bus>> c2toji() throws Exception {
		
		List<Bus> bus = busService.getC2toJi();
		
		if(bus.isEmpty()) {
			return new ResponseEntity<List<Bus>>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<List<Bus>>(bus, HttpStatus.OK);
	}
	
	@ApiOperation(response=Bus.class, value = "2캠퍼스 -> 일산 버스 조회")
	@RequestMapping(value = "/api/bus/c2toil", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<Bus>> c2toil() throws Exception {
		
		List<Bus> bus = busService.getC2toIl();
		
		if(bus.isEmpty()) {
			return new ResponseEntity<List<Bus>>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<List<Bus>>(bus, HttpStatus.OK);
	}
	
	@ApiOperation(response=Bus.class, value = "일산 -> 2캠퍼스 버스 조회")
	@RequestMapping(value = "/api/bus/iltoc2", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<Bus>> iltoc2() throws Exception {
		
		List<Bus> bus = busService.getIltoC2();
		
		if(bus.isEmpty()) {
			return new ResponseEntity<List<Bus>>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<List<Bus>>(bus, HttpStatus.OK);
	}
	
	@ApiOperation(response=Bus.class, value = "지행 -> 2캠퍼스 버스 조회")
	@RequestMapping(value = "/api/bus/jitoc2", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<Bus>> jitoc2() throws Exception {
		
		List<Bus> bus = busService.getJitoC2();
		
		if(bus.isEmpty()) {
			return new ResponseEntity<List<Bus>>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<List<Bus>>(bus, HttpStatus.OK);
	}

}
