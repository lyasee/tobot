package com.tobot.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tobot.domain.Keyboard;
import com.tobot.service.KeyboardService;

@RestController
public class KeyboardController {

	private static final Logger logger = LoggerFactory.getLogger(KeyboardController.class);
	
	@Autowired
	private KeyboardService keyboardService;
	
	// 카카오 홈에 접속시 처음으로 실행되는 API, 메인버튼 반환
	@RequestMapping(value = "/keyboard", method = RequestMethod.GET)
	public ResponseEntity<Keyboard> keyboard() throws Exception {
		
		Keyboard keyboard = keyboardService.init();
		
		return new ResponseEntity<Keyboard>(keyboard, HttpStatus.OK);
	}
	
}
