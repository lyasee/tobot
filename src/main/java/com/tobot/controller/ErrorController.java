package com.tobot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ErrorController {
	String path = "/error"; 
	
	@RequestMapping(value = "/404") 
	public String error404() { 
		System.out.println("error controller called");
		return path + "/404"; 
	}
	
	@RequestMapping(value = "/500") 
	public String error500() { 
		System.out.println("error controller called");
		return path + "/500"; 
	}
	
}
