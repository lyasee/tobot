package com.tobot.controller;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tobot.domain.Board;
import com.tobot.domain.Criteria;
import com.tobot.domain.PageMaker;
import com.tobot.domain.SearchCriteria;
import com.tobot.domain.User;
import com.tobot.service.NoticeBoardService;

@Controller
@RequestMapping("/board")
public class BoardNoticeController {

	private static final Logger logger = LoggerFactory.getLogger(BoardNoticeController.class);
	
	@Inject
	private NoticeBoardService service;
	
	@RequestMapping(value="/notice", method=RequestMethod.GET)
	public String noticeBoard(@ModelAttribute("cri") Criteria cri, HttpSession session, Model model) throws Exception {
		User sess = (User) session.getAttribute("login");

		logger.info(cri.toString());

	    PageMaker pageMaker = new PageMaker();
	    pageMaker.setCri(cri);
	    
	    pageMaker.setTotalCount(service.listCountCriteria(cri));

	    model.addAttribute("list", service.listCriteria(cri));    
	    model.addAttribute("pageMaker", pageMaker);
		model.addAttribute("login", sess);
		
		return "/board/noticeBoard";
	}
	
	@RequestMapping(value="/writePage", method=RequestMethod.GET)
	public void writePage(Model model) {		
	}
	
	@RequestMapping(value="/writePage", method=RequestMethod.POST)
	public String writePost(Board board, HttpSession session, Model model) throws Exception {
		service.insert(board);

	    return "redirect:/board/notice";
	}
	
	@RequestMapping(value = "/readPage", method = RequestMethod.GET)
	public void read(@RequestParam("idx") int idx, @ModelAttribute("cri") SearchCriteria cri, Model model) throws Exception {
	
		model.addAttribute(service.read(idx));
	}


	@RequestMapping(value = "/modifyPage", method = RequestMethod.GET)
	public void modifyPagingGET(int idx, @ModelAttribute("cri") SearchCriteria cri, Model model) throws Exception {
	
		model.addAttribute(service.read(idx));
	}

	@RequestMapping(value = "/modifyPage", method = RequestMethod.POST)
	public String modifyPagingPOST(Board board, SearchCriteria cri, RedirectAttributes rttr) throws Exception {
	
		logger.info(cri.toString());
		logger.info(board.toString());
		service.modify(board);
	
	    rttr.addAttribute("page", cri.getPage());
		rttr.addAttribute("perPageNum", cri.getPerPageNum());
		rttr.addAttribute("searchType", cri.getSearchType());
		rttr.addAttribute("keyword", cri.getKeyword());
	
		rttr.addFlashAttribute("msg", "SUCCESS");
		
		logger.info(rttr.toString());
		
	    return "redirect:/board/notice";
	}
	  
	@RequestMapping(value = "/removePage", method = RequestMethod.POST)
	public String remove(@RequestParam("idx") int idx, SearchCriteria cri, RedirectAttributes rttr) throws Exception {

		service.remove(idx);
		
		rttr.addAttribute("page", cri.getPage());
		rttr.addAttribute("perPageNum", cri.getPerPageNum());
		rttr.addAttribute("searchType", cri.getSearchType());
		rttr.addAttribute("keyword", cri.getKeyword());
		
		rttr.addFlashAttribute("msg", "SUCCESS");

	    return "redirect:/board/notice";
	}
}
