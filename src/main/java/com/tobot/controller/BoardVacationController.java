package com.tobot.controller;

import java.util.List;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tobot.domain.Bus;
import com.tobot.service.BusService;

@RequestMapping("/board/bus")
@Controller
public class BoardVacationController {
	
	@Inject
	private BusService service;
	
	//-------------  C2 -> JI 게시판 맵핑 -------------
	
	//게시판 목록
	@RequestMapping(value = "/vacation/c2jiList", method = RequestMethod.GET)
	public void c2tojiList(Model model) throws Exception {
		
		List<Bus> bus = service.getAllC1toC2();
		
		model.addAttribute("busList", bus);
	}
	
	//시간표 삽입 
	@RequestMapping(value = "/vacation/c2jiInsert", method = RequestMethod.GET)
	public void c2tojiInsert(Model model) throws Exception {
		
	}
	
	//시간표 수정
	@RequestMapping(value = "/vacation/c2jiModify", method = RequestMethod.GET)
	public void c2tojiModifyList(Model model) throws Exception {

		List<Bus> bus = service.getAllC1toC2();
		
		model.addAttribute("busList", bus);
	}
	
	//시간표 삽입 요청
	@RequestMapping(value = "/vacation/c2jiInsert", method = RequestMethod.POST)
	public String c2tojiInsert(Bus vo, RedirectAttributes rttr) throws Exception {
		System.out.println("vo ="+vo);
		
		service.insertC1toC2(vo);
		service.sortTable("c1toc2");

	    rttr.addFlashAttribute("code", "101"); //삽입성공 메세지

	    return "redirect:/board/bus/c1c2/c1c2List";
	}
	
	//시간표 수정 요청 AJAX
	@RequestMapping(value = "/vacation/c2jiModify", method = RequestMethod.POST)
	public ResponseEntity<String> c2tojiModify(Bus vo, Model model) throws Exception {

		ResponseEntity<String> entity = null;
		
		try {
			service.updateC1toC2(vo);
			service.sortTable("c1toc2");
			entity = new ResponseEntity<>("SUCCESS", HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			entity = new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return entity;
	}
	
	//시간표 삭제 요청 AJAX
	@RequestMapping(value = "/vacation/c2jiDelete", method = RequestMethod.POST)
	public ResponseEntity<String> c2tojiDelete(@RequestParam("idx") int idx, Model model) throws Exception {
		
		ResponseEntity<String> entity = null;
		
		try {
			service.deleteC1toC2(idx);
			service.sortTable("c1toc2");
			entity = new ResponseEntity<>("SUCCESS", HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			entity = new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return entity;
	}
	
	//-------------  JI -> C2 게시판 맵핑 -------------
	
	//게시판 목록
	@RequestMapping(value = "/vacation/jic2List", method = RequestMethod.GET)
	public void jitoc2List(Model model) throws Exception {
		
		List<Bus> bus = service.getAllC1toC2();
		
		model.addAttribute("busList", bus);
	}
	
	//시간표 삽입 
	@RequestMapping(value = "/vacation/jic2Insert", method = RequestMethod.GET)
	public void jitoc2Insert(Model model) throws Exception {
		
	}
	
	//시간표 수정
	@RequestMapping(value = "/vacation/jic2Modify", method = RequestMethod.GET)
	public void jitoc2ModifyList(Model model) throws Exception {

		List<Bus> bus = service.getAllC1toC2();
		
		model.addAttribute("busList", bus);
	}
	
	//시간표 삽입 요청
	@RequestMapping(value = "/vacation/jic2Insert", method = RequestMethod.POST)
	public String jitoc2Insert(Bus vo, RedirectAttributes rttr) throws Exception {
		System.out.println("vo ="+vo);
		
		service.insertC1toC2(vo);
		service.sortTable("c1toc2");

	    rttr.addFlashAttribute("code", "101"); //삽입성공 메세지

	    return "redirect:/board/bus/c1c2/c1c2List";
	}
	
	//시간표 수정 요청 AJAX   
	@RequestMapping(value = "/vacation/jic2Modify", method = RequestMethod.POST)
	public ResponseEntity<String> jitoc2Modify(Bus vo, Model model) throws Exception {

		ResponseEntity<String> entity = null;
		
		try {
			service.updateC1toC2(vo);
			service.sortTable("c1toc2");
			entity = new ResponseEntity<>("SUCCESS", HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			entity = new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return entity;
	}
	
	//시간표 삭제 요청 AJAX
	@RequestMapping(value = "/vacation/jic2Delete", method = RequestMethod.POST)
	public ResponseEntity<String> jitoc2Delete(@RequestParam("idx") int idx, Model model) throws Exception {
		
		ResponseEntity<String> entity = null;
		
		try {
			service.deleteC1toC2(idx);
			service.sortTable("c1toc2");
			entity = new ResponseEntity<>("SUCCESS", HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			entity = new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return entity;
	}
}
