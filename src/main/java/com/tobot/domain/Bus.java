package com.tobot.domain;

import java.util.Date;

public class Bus {

	private int idx;
	private String time_str;
	private Date time;
	private String arrival_time_str;
	private Date arrival_time;
	
	public int getIdx() {
		return idx;
	}
	
	public void setIdx(int idx) {
		this.idx = idx;
	}
	
	public String getTime_str() {
		return time_str;
	}
	
	public void setTime_str(String time_str) {
		this.time_str = time_str;
	}
	
	public Date getTime() {
		return time;
	}
	
	public void setTime(Date time) {
		this.time = time;
	}
	
	public String getArrival_time_str() {
		return arrival_time_str;
	}

	public void setArrival_time_str(String arrival_time_str) {
		this.arrival_time_str = arrival_time_str;
	}

	public Date getArrival_time() {
		return arrival_time;
	}

	public void setArrival_time(Date arrival_time) {
		this.arrival_time = arrival_time;
	}

	@Override
	public String toString() {
		return "idx: " + idx + "\n" + "time_str: " + time_str + "\n" + "time: " + time;
	}
	
}
