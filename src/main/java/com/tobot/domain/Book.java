package com.tobot.domain;

public class Book {
	
	private String title; // 제목
	private String author; // 저자
	private String publish; // 출판사
	private String year; // 출판일
	private String collno; // 
	private String campus; // 캠퍼스 구분
	private String code; // 책의 코드, 조회 용
	
	private String barcode;
	private String volCopy;
	private String place;
	private String state;
	private String reservebtn;
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getAuthor() {
		return author;
	}
	
	public void setAuthor(String author) {
		this.author = author;
	}
	
	public String getPublish() {
		return publish;
	}
	
	public void setPublish(String publish) {
		this.publish = publish;
	}
	
	public String getYear() {
		return year;
	}
	
	public void setYear(String year) {
		this.year = year;
	}
	
	public String getCollno() {
		return collno;
	}
	
	public void setCollno(String collno) {
		this.collno = collno;
	}
	
	public String getCampus() {
		return campus;
	}
	
	public void setCampus(String campus) {
		this.campus = campus;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getVolCopy() {
		return volCopy;
	}

	public void setVolCopy(String volCopy) {
		this.volCopy = volCopy;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getReservebtn() {
		return reservebtn;
	}

	public void setReservebtn(String reservebtn) {
		this.reservebtn = reservebtn;
	}

}
