package com.tobot.domain;

import java.util.Date;

public class Board {
	private Integer idx;
	private String title;
	private String content;
	private String writer;
	private Date regdate;;
	private int viewcnt;
	private int replycnt;
	
	
	public int getReplycnt() {
		return replycnt;
	}
	public void setReplycnt(int replycnt) {
		this.replycnt = replycnt;
	}
	public Integer getIdx() {
		return idx;
	}
	public void setIdx(Integer idx) {
		this.idx = idx;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getWriter() {
		return writer;
	}
	public void setWriter(String writer) {
		this.writer = writer;
	}
	public Date getRegdate() {
		return regdate;
	}
	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}
	public int getViewcnt() {
		return viewcnt;
	}
	public void setViewcnt(int viewcnt) {
		this.viewcnt = viewcnt;
	}


	@Override
	public String toString() {
		return "BoardVO [idx= "+idx +", title= "+title+", content= "+content+", writer= "+writer+", regdate= "+regdate+", viewcnt= "+viewcnt+ "]";
	}

	
	
	
}
