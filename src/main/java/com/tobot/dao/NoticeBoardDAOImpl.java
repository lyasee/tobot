package com.tobot.dao;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.tobot.domain.Board;
import com.tobot.domain.Criteria;
import com.tobot.domain.SearchCriteria;

@Repository
public class NoticeBoardDAOImpl implements NoticeBoardDAO {

	private static final String namespace="com.tobot.mapper.BoardMapper";
	
	@Inject
	private SqlSession session;

	
	@Override
	public void insert(Board board) throws Exception {
		session.insert(namespace+".insert", board);		
	}

	@Override
	public Board read(Integer idx) throws Exception {
		return session.selectOne(namespace+".read", idx);
	}

	@Override
	public void modify(Board board) throws Exception {
		session.update(namespace+".update", board);
	}

	@Override
	public void remove(Integer idx) throws Exception {
		session.delete(namespace+".delete", idx);
	}

	@Override
	public List<Board> listCriteria(Criteria cri) throws Exception {
		return session.selectList(namespace+".listCriteria", cri);
	}

	@Override
	public int countPaging(Criteria cri) throws Exception {
		return session.selectOne(namespace+".countPaging", cri);
	}

	@Override
	public int listSearchCount(SearchCriteria cri) throws Exception {
		return session.selectOne(namespace+".listSearchCount", cri);
	
	}
}
