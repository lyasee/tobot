package com.tobot.dao;

import java.util.List;

import com.tobot.domain.Bus;

public interface BusDao {
	
	/**
	 * 1캠퍼스 -> 2캠퍼스, 전체 데이터
	 * @param time
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getAllC1toC2() throws Exception;
	
	/**
	 * 1캠퍼스 -> 일산, 전체 데이터
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getAllC1toIl() throws Exception;
	
	/**
	 * 1캠퍼스 -> 지행, 전체 데이터
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getAllC1toJi() throws Exception;
	
	/**
	 * 2캠퍼스 -> 1캠퍼스, 전체 데이터
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getAllC2toC1() throws Exception;
	
	/**
	 * 2캠퍼스 -> 일산, 전체 데이터
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getAllC2toIl() throws Exception;
	
	/**
	 * 일산 -> 2캠퍼스, 전체 데이터
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getAllIltoC2() throws Exception;
	
	/**
	 * 지행 -> 2캠퍼스, 전체 데이터
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getAllJitoC2() throws Exception;
	
	/**
	 * 2캠퍼스 -> 지행, 전체 데이터
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getAllC2toJi() throws Exception;
	
	// 현재 시간에 맞게 2개만 가져오기
	/**
	 * 1캠퍼스 -> 2캠퍼스, 데이터 2개
	 * @param time
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getC1toC2(String time) throws Exception;
	
	/**
	 * 1캠퍼스 -> 일산, 데이터 2개
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getC1toIl(String time) throws Exception;

	/**
	 * 1캠퍼스 -> 지행, 데이터 2개
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getC1toJi(String time) throws Exception;

	/**
	 * 2캠퍼스 -> 1캠퍼스, 데이터 2개
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getC2toC1(String time) throws Exception;

	/**
	 * 2캠퍼스 -> 일산, 데이터 2개
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getC2toIl(String time) throws Exception;

	/**
	 * 일산 -> 2캠퍼스, 데이터 2개
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getIltoC2(String time) throws Exception;

	/**
	 * 지행 -> 2캠퍼스, 데이터 2개
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getJitoC2(String time) throws Exception;
	
	/**
	 * 2캠퍼스 -> 지행, 데이터 2개
	 * @return List<Bus>
	 * @throws Exception
	 */
	public List<Bus> getC2toJi(String time) throws Exception;
	
	// ----------- 시간표 삽입 ------------
	/**
	 * 2캠퍼스 -> 지행 시간표 삽입
	 * @param Bus
	 * @throws Exception
	 */
	public void insertC2toJi(Bus vo) throws Exception; 
	
	/**
	 * 2캠퍼스 -> 지행 시간표 삽입
	 * @param Bus
	 * @throws Exception
	 */
	public void insertJitoC2(Bus vo) throws Exception;
	
	/**
	 * 2캠퍼스 -> 지행 시간표 삽입
	 * @param Bus
	 * @throws Exception
	 */
	public void insertC1toC2(Bus vo) throws Exception;
	
	/**
	 * 2캠퍼스 -> 지행 시간표 삽입
	 * @param Bus
	 * @throws Exception
	 */
	public void insertC2toC1(Bus vo) throws Exception;
	
	/**
	 * 2캠퍼스 -> 지행 시간표 삽입
	 * @param Bus
	 * @throws Exception
	 */
	public void insertC2toIl(Bus vo) throws Exception;
	
	/**
	 * 2캠퍼스 -> 지행 시간표 삽입
	 * @param Bus
	 * @throws Exception
	 */
	public void insertIltoC2(Bus vo) throws Exception;
	
	/**
	 * 2캠퍼스 -> 지행 시간표 삽입
	 * @param Bus
	 * @throws Exception
	 */
	public void insertC1toIl(Bus vo) throws Exception;
	
	
	
	// ----------- 시간표 수정 ------------
	/**
	 * 2캠퍼스 -> 지행 시간표 수정
	 * @param Bus
	 * @throws Exception
	 */
	public void updateC2toJi(Bus vo) throws Exception;
	/**
	 * 2캠퍼스 -> 지행 시간표 수정
	 * @param Bus
	 * @throws Exception
	 */
	public void updateJitoC2(Bus vo) throws Exception;
	/**
	 * 2캠퍼스 -> 지행 시간표 수정
	 * @param Bus
	 * @throws Exception
	 */
	public void updateC1toC2(Bus vo) throws Exception;
	/**
	 * 2캠퍼스 -> 지행 시간표 수정
	 * @param Bus
	 * @throws Exception
	 */
	public void updateC2toC1(Bus vo) throws Exception;
	/**
	 * 2캠퍼스 -> 지행 시간표 수정
	 * @param Bus
	 * @throws Exception
	 */
	public void updateC2toIl(Bus vo) throws Exception;
	/**
	 * 2캠퍼스 -> 지행 시간표 수정
	 * @param Bus
	 * @throws Exception
	 */
	public void updateIltoC2(Bus vo) throws Exception;
	/**
	 * 2캠퍼스 -> 지행 시간표 수정
	 * @param Bus
	 * @throws Exception
	 */
	public void updateC1toIl(Bus vo) throws Exception;
	

	// ----------- 시간표 삭제 ------------
	/**
	 * 2캠퍼스 -> 지행 시간표 삭제
	 * @param int
	 * @throws Exception
	 */
	public void deleteC2toJi(int idx) throws Exception;
	/**
	 * 2캠퍼스 -> 지행 시간표 삭제
	 * @param int
	 * @throws Exception
	 */
	public void deleteJitoC2(int idx) throws Exception;
	/**
	 * 2캠퍼스 -> 지행 시간표 삭제
	 * @param int
	 * @throws Exception
	 */
	public void deleteC1toC2(int idx) throws Exception;
	/**
	 * 2캠퍼스 -> 지행 시간표 삭제
	 * @param int
	 * @throws Exception
	 */
	public void deleteC2toC1(int idx) throws Exception;
	/**
	 * 2캠퍼스 -> 지행 시간표 삭제
	 * @param int
	 * @throws Exception
	 */
	public void deleteC2toIl(int idx) throws Exception;
	/**
	 * 2캠퍼스 -> 지행 시간표 삭제
	 * @param int
	 * @throws Exception
	 */
	public void deleteIltoC2(int idx) throws Exception;
	/**
	 * 2캠퍼스 -> 지행 시간표 삭제
	 * @param int
	 * @throws Exception
	 */
	public void deleteC1toIl(int idx) throws Exception;

	// ----------- 시간표 테이블 삭제 ------------
	/**
	 * 2캠퍼스 -> 지행 테이블 삭제
	 * @param int
	 * @throws Exception
	 */
	public void deleteAllC2toJi() throws Exception;
	/**
	 * 2캠퍼스 -> 지행 테이블 삭제
	 * @param int
	 * @throws Exception
	 */
	public void deleteAllJitoC2() throws Exception;
	/**
	 * 2캠퍼스 -> 지행 테이블 삭제
	 * @param int
	 * @throws Exception
	 */
	public void deleteAllC1toC2() throws Exception;
	/**
	 * 2캠퍼스 -> 지행 테이블 삭제
	 * @param int
	 * @throws Exception
	 */
	public void deleteAllC2toC1() throws Exception;
	/**
	 * 2캠퍼스 -> 지행 테이블 삭제
	 * @param int
	 * @throws Exception
	 */
	public void deleteAllC2toIl() throws Exception;
	/**
	 * 2캠퍼스 -> 지행 테이블 삭제
	 * @param int
	 * @throws Exception
	 */
	public void deleteAllIltoC2() throws Exception;
	/**
	 * 2캠퍼스 -> 지행 테이블 삭제
	 * @param int
	 * @throws Exception
	 */
	public void deleteAllC1toIl() throws Exception;

}
