package com.tobot.dao;

import java.util.List;

import com.tobot.domain.Board;
import com.tobot.domain.Criteria;
import com.tobot.domain.SearchCriteria;

public interface NoticeBoardDAO {
	public void  insert(Board board) throws Exception;
	
	public Board read(Integer idx) throws Exception;
	
	public void modify(Board board) throws Exception;
	
	public void remove(Integer idx) throws Exception;
	
	public List<Board> listCriteria(Criteria cri) throws Exception;
	
	public int countPaging(Criteria cri) throws Exception;
	  
	public int listSearchCount(SearchCriteria cri) throws Exception;
}
