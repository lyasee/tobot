package com.tobot.dao;

import com.tobot.domain.User;

public interface UserDAO {
	public User login(User vo) throws Exception;
}
