package com.tobot.dao;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.tobot.domain.User;

@Repository
public class UserDAOImpl implements UserDAO {
	@Inject
	private SqlSession session;

	private static String namespace = "com.tobot.mapper.UserMapper";

	@Override
	public User login(User vo) throws Exception {
		return session.selectOne(namespace+".login", vo);
	}

}
