package com.tobot.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tobot.domain.Bus;

@Repository
public class BusDaoImpl implements BusDao{
	
	@Autowired
	private SqlSession sqlSession;
	
	private static final String namespace = "com.lyasee.ddobot.mapper.BusMapper";

	@Override
	public List<Bus> getAllC1toC2() throws Exception {
		return sqlSession.selectList(namespace  + ".getAllC1toC2");
	}

	@Override
	public List<Bus> getAllC1toIl() throws Exception {
		return sqlSession.selectList(namespace  + ".getAllC1toIl");
	}

	@Override
	public List<Bus> getAllC1toJi() throws Exception {
		return sqlSession.selectList(namespace  + ".getAllC1toJi");
	}

	@Override
	public List<Bus> getAllC2toC1() throws Exception {
		return sqlSession.selectList(namespace  + ".getAllC2toC1");
	}

	@Override
	public List<Bus> getAllC2toIl() throws Exception {
		return sqlSession.selectList(namespace  + ".getAllC2toIl");
	}

	@Override
	public List<Bus> getAllIltoC2() throws Exception {
		return sqlSession.selectList(namespace  + ".getAllIltoC2");
	}

	@Override
	public List<Bus> getAllJitoC2() throws Exception {
		return sqlSession.selectList(namespace  + ".getAllJitoC2");
	}
	
	@Override
	public List<Bus> getAllC2toJi() throws Exception {
		return sqlSession.selectList(namespace  + ".getAllC2toJi");
	}

	@Override
	public List<Bus> getC1toC2(String time) throws Exception {
		return sqlSession.selectList(namespace  + ".getC1toC2", time);
	}

	@Override
	public List<Bus> getC1toIl(String time) throws Exception {
		return sqlSession.selectList(namespace  + ".getC1toIl", time);
	}

	@Override
	public List<Bus> getC1toJi(String time) throws Exception {
		return sqlSession.selectList(namespace  + ".getC1toJi", time);
	}

	@Override
	public List<Bus> getC2toC1(String time) throws Exception {
		return sqlSession.selectList(namespace  + ".getC2toC1", time);
	}

	@Override
	public List<Bus> getC2toIl(String time) throws Exception {
		return sqlSession.selectList(namespace  + ".getC2toIl", time);
	}

	@Override
	public List<Bus> getIltoC2(String time) throws Exception {
		return sqlSession.selectList(namespace  + ".getIltoC2", time);
	}

	@Override
	public List<Bus> getJitoC2(String time) throws Exception {
		return sqlSession.selectList(namespace  + ".getJitoC2", time);
	}

	@Override
	public List<Bus> getC2toJi(String time) throws Exception {
		return sqlSession.selectList(namespace  + ".getC2toJi", time);
	}
	
	// ------------ 삽입 ------------  
	@Override
	public void insertC2toJi(Bus vo) throws Exception {
		sqlSession.insert(namespace + ".insertC2toJI", vo);
	}

	@Override
	public void insertJitoC2(Bus vo) throws Exception {
		sqlSession.insert(namespace + ".insertJitoC2", vo);
	}

	@Override
	public void insertC1toC2(Bus vo) throws Exception {
		sqlSession.insert(namespace + ".insertC1toC2", vo);
	}

	@Override
	public void insertC2toC1(Bus vo) throws Exception {
		sqlSession.insert(namespace + ".insertC2toC1", vo);
	}

	@Override
	public void insertC2toIl(Bus vo) throws Exception {
		sqlSession.insert(namespace + ".insertC2toIl", vo);
	}

	@Override
	public void insertIltoC2(Bus vo) throws Exception {
		sqlSession.insert(namespace + ".insertIltoC2", vo);
	}

	@Override
	public void insertC1toIl(Bus vo) throws Exception {
		sqlSession.insert(namespace + ".insertC1toIl", vo);
	}

	// ------------ 수정 ------------  
	@Override
	public void updateC2toJi(Bus vo) throws Exception {
		sqlSession.update(namespace  + ".updateC2toJI", vo);		
	}
	
	@Override
	public void updateJitoC2(Bus vo) throws Exception {
		sqlSession.update(namespace  + ".updateJitoC2", vo);	
	}

	@Override
	public void updateC1toC2(Bus vo) throws Exception {
		sqlSession.update(namespace  + ".updateC1toC2", vo);	
	}

	@Override
	public void updateC2toC1(Bus vo) throws Exception {
		sqlSession.update(namespace  + ".updateC2toC1", vo);	
	}

	@Override
	public void updateC2toIl(Bus vo) throws Exception {
		sqlSession.update(namespace  + ".updateC2toIl", vo);	
	}

	@Override
	public void updateIltoC2(Bus vo) throws Exception {
		sqlSession.update(namespace  + ".updateIltoC2", vo);	
	}

	@Override
	public void updateC1toIl(Bus vo) throws Exception {
		sqlSession.update(namespace  + ".updateC1toIl", vo);	
	}
	
	// ------------ 삭제 ------------ 
	@Override
	public void deleteC2toJi(int idx) throws Exception {
		sqlSession.delete(namespace + ".deleteC2toJI", idx);
	}
	
	@Override
	public void deleteJitoC2(int idx) throws Exception {
		sqlSession.delete(namespace + ".deleteJitoC2", idx);
	}

	@Override
	public void deleteC1toC2(int idx) throws Exception {
		sqlSession.delete(namespace + ".deleteC1toC2", idx);
	}

	@Override
	public void deleteC2toC1(int idx) throws Exception {
		sqlSession.delete(namespace + ".deleteC2toC1", idx);
	}

	@Override
	public void deleteC2toIl(int idx) throws Exception {
		sqlSession.delete(namespace + ".deleteC2toIl", idx);
	}

	@Override
	public void deleteIltoC2(int idx) throws Exception {
		sqlSession.delete(namespace + ".deleteIltoC2", idx);
	}

	@Override
	public void deleteC1toIl(int idx) throws Exception {
		sqlSession.delete(namespace + ".deleteC1toIl", idx);
	}
	
	// ------------ 테이블 삭제(정렬) ------------
	@Override
	public void deleteAllC2toJi() throws Exception {
		sqlSession.delete(namespace + ".deleteAllC2toJI");		
	}
	
	@Override
	public void deleteAllJitoC2() throws Exception {
		sqlSession.delete(namespace + ".deleteAllJitoC2");		
	}

	@Override
	public void deleteAllC1toC2() throws Exception {
		sqlSession.delete(namespace + ".deleteAllC1toC2");		
	}

	@Override
	public void deleteAllC2toC1() throws Exception {
		sqlSession.delete(namespace + ".deleteAllC2toC1");		
	}

	@Override
	public void deleteAllC2toIl() throws Exception {
		sqlSession.delete(namespace + ".deleteAllC2toIl");		
	}

	@Override
	public void deleteAllIltoC2() throws Exception {
		sqlSession.delete(namespace + ".deleteAllIltoC2");
	}

	@Override
	public void deleteAllC1toIl() throws Exception {
		sqlSession.delete(namespace + ".deleteAllC1toIl");		
	}

}
