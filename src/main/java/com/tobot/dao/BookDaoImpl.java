package com.tobot.dao;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Repository;

import com.tobot.domain.Book;

@Repository
public class BookDaoImpl implements BookDao {

	@Override
	public List<Book> findBook(String name) throws Exception {
		
		List<Book> books = new ArrayList<Book>();
		Book book;
		String bookCode = "";
		int start = 0;
		int end = 0;
		String code = "";
		
		String URL = "http://lib.shinhan.ac.kr/lib/Search/SearchListC.csp";
				URL = URL + "?HLOC=";
				URL = URL + "&COUNT=881Xm9KkKK";
				URL = URL + "&Kor=1";
				URL = URL + "&New=C0";
				URL = URL + "&Gate=DA";
				URL = URL + "&TARGET=0";
				URL = URL + "&SrchTarget=B";
				URL = URL + "&SrchType=0";
				URL = URL + "&SrchType01=T";
				URL = URL + "&SrchKey01="+ (name); // encodeURIComponent 필요
				URL = URL + "&SrchCondi01=%E2%96%B2";
				URL = URL + "&SrchClass=All";
				URL = URL + "&GTYPE0=on";
				URL = URL + "&STYPE0=ALL";
				URL = URL + "&x=0";
				URL = URL + "&y=0";
		       
		Document doc = Jsoup.connect(URL).get();
		Elements element = doc.select("td.book_titletd");
		
		for(Element ele : element) {
			book = new Book();
			book.setTitle(ele.select(".book_title > a").text());
			book.setAuthor(ele.select(".book_author").text());
			book.setPublish(ele.select(".book_publish").text());
			book.setYear(ele.select(".book_year").text());
			book.setCollno(ele.select(".book_collno").text());
			book.setCampus(ele.select("li:nth-child(4) > a > span").text());
			
			bookCode = ele.select(".book_title > a").attr("href");
			start = bookCode.indexOf("(\'");
			end = bookCode.indexOf("\',", start+2);
			code = bookCode.substring(start+2, end);
			book.setCode(code);
			
			books.add(book);
		}
		
		return books;
	}

	@Override
	public Book bookDetail(String code, String campus) throws Exception {
		
		Book book = new Book();
		
		String loc = "";
		
		if(campus.contains("중앙도서관(제1캠퍼스)")) {
			loc = "SHC";
		}else if(campus.contains("도서관(제2캠퍼스)")) {
			loc = "HBLIB";
		}
		
		String URL = "http://lib.shinhan.ac.kr/lib/Search/SearchReport.csp";
				URL = URL + "?HLOC=SHC";
				URL = URL + "&COUNT=881Xm9KkKK";
				URL = URL + "&Kor=1";
				URL = URL + "&SrchTarget=B";
				URL = URL + "&SrchClearOption=NC";
				URL = URL + "&LOC=" + loc;
				URL = URL + "&FILENUM=" + code;
		       
		Document doc = Jsoup.connect(URL).get();
		Elements element = doc.select("#BookOwned > div.bibliograph_info > table > tbody > tr");
		
		for(Element ele : element) {
			book = new Book();
			book.setBarcode(ele.select("td:nth-child(1)").text());
			book.setVolCopy(ele.select("td:nth-child(2)").text());
			book.setPlace(ele.select("td:nth-child(4)").text());
			book.setState(ele.select("td:nth-child(5)").text().trim());
			book.setReservebtn(ele.select("td:nth-child(6)").text());
		}
		
		return book;
	}
	
}
