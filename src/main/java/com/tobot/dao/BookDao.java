package com.tobot.dao;

import java.util.List;

import com.tobot.domain.Book;

public interface BookDao {
	
	public List<Book> findBook(String name) throws Exception;
	
	public Book bookDetail(String code, String campus) throws Exception;

}
