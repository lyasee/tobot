<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@include file="../include/header.jsp"%>

<!-- 컨텐츠  Wrapper. -->
<div class="content-wrapper">
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-12">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title">게시글 수정</h3>
					</div>
					<!-- /.box-header -->
	
					<form action="/board/modifyPage" method="post">					
						<input type='hidden' name='page' value="${cri.page}">
						<input type='hidden' name='perPageNum' value="${cri.perPageNum}">
						<input type='hidden' name='searchType' value="${cri.searchType}">
						<input type='hidden' name='keyword' value="${cri.keyword}">
	
						<div class="box-body">
	
							<div class="form-group">
								<label for="exampleInputEmail1">글 번호</label> 
								<input type="text" name='idx' class="form-control" value="${board.idx}" readonly="readonly">
							</div>
	
							<div class="form-group">
								<label for="exampleInputEmail1">제목</label>
								 <input type="text" name='title' class="form-control" value="${board.title}">
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">본문</label>
								<textarea class="form-control" name="content" rows="3">${board.content}</textarea>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">작성자</label>
								<input type="text" name="writer" class="form-control" value="${board.writer}" readonly="readonly">
							</div>
						</div>
						<!-- /.box-body -->
						<div class="box-footer">
							<button type="submit" class="btn btn-primary">저장</button>
							<button type="button" class="btn btn-warning" >취소</button>
						</div>
					</form>
	
	<script>
/* 	$(document).ready(
		function() {
	
			var formObj = $("form[role='form']");
	
			console.log(formObj);
	
			$(".btn-warning")
					.on("click",function() {
						self.location = "/sboard/list?page=${cri.page}&perPageNum=${cri.perPageNum}"
								+ "&searchType=${cri.searchType}&keyword=${cri.keyword}";
					});
	
			$(".btn-primary").on("click",
					function() {
						formObj.submit();
					});
		}); */
	</script>
	
	
	
	
				</div>
				<!-- /.box -->
			</div>
			<!--/.col (left) -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<%@include file="../include/footer.jsp"%>
