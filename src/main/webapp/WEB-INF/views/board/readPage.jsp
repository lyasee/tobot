<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="../include/header.jsp"%>

<script>


</script>
<!-- Main content -->
<!-- 컨텐츠  Wrapper. -->
<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-12">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title">게시글 상세보기</h3>
					</div>
					<!-- /.box-header -->
	
					<form role="form" action="modifyPage" method="post">
						<input type='hidden' name='bno' value="${boardVO.bno}">
						<input type='hidden' name='page' value="${cri.page}"> 
						<input type='hidden' name='perPageNum' value="${cri.perPageNum}">
						<input type='hidden' name='searchType' value="${cri.searchType}">
						<input type='hidden' name='keyword' value="${cri.keyword}">
					</form>
	
					<div class="box-body">
						<div class="form-group">
							<label for="exampleInputEmail1">제목</label>
							<input type="text" name='title' class="form-control" value="${board.title}" readonly="readonly">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">작성자</label> 
							<input type="text" name="writer" class="form-control" value="${board.writer}" readonly="readonly">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">본문</label>
							<textarea class="form-control" name="content" rows="3" readonly="readonly">${board.content}</textarea>
						</div>
					</div>
					<!-- /.box-body -->
					
			  		<div class="box-footer">
			    
						<c:if test="${login.idx == boardVO.writer}">
						   	<button type="button" class="btn btn-warning" id="modifyBtn" onclick="location.href='/board/modifyPage?idx=${board.idx}'">수정</button>
						   	<form action="/board/removePage" method="post">
						   		<input type="hidden" value="${board.idx }" name="idx">
								<button type="submit" class="btn btn-danger" id="removeBtn">삭제</button>
						   	</form>
						</c:if>
					   	<button type="button" class="btn btn-primary" id="goListBtn" onclick="location.href='/board/notice'">목록 </button>
					</div>
				</div>
				<!-- /.box -->
			</div>
			<!--/.col (left) -->
		</div>
		<!-- /.row -->
		
		<!-- Modal -->
		<div id="modifyModal" class="modal modal-primary fade" role="dialog">
		  <div class="modal-dialog">
		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title"></h4>
		      </div>
		      <div class="modal-body" data-rno>
		        <p><input type="text" id="replytext" class="form-control"></p>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-info" id="replyModBtn">수정</button>
		        <button type="button" class="btn btn-danger" id="replyDelBtn">삭제</button>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      </div>
		    </div>
		  </div>
		</div>      
		
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->


<%@include file="../include/footer.jsp"%>
