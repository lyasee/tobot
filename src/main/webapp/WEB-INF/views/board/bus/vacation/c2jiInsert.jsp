<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@include file="../../../include/header.jsp"%>

<script>
function validateTime(time) {
	const re = /[0-2][0-9]:[0-5][0-9]/;
	return re.test(time);
}

$(document).ready(function() {
	document.getElementById('btn').onclick = function(){
		let time_str = $('#time_str').val();
		let arrival_time_str = $('#arrival_time_str').val();
		
		if(validateTime(time_str)&&validateTime(arrival_time_str)){
			document.getElementById('frm').submit();
		} else {
			alert("시간값을 정확하게 입력하세요");
		}
		
		return false;
	};
});

</script>
<!-- 컨텐츠  Wrapper. -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>방학시간표 (2캠->지행)</h1>
		<ol class="breadcrumb">
	    <li><a href="/main"><i class="fa fa-dashboard"></i> 메인화면</a></li>
	    <li><a href="#">버스 시간표 관리</a></li>
        <li class="active">방학시간표 (2캠->지행)</li>
        <li class="active">시간표 추가</li>
		</ol>
	</section>
 <!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">시간표</h3>
				</div>
				<!-- /.box-header -->

				<form role="form" id="frm" method="post" action="/board/bus/vacation/c2jiInsert">
					<div class="box-body">
						<table class="table table-bordered">
							<tr>
								<th align="center">출발시간</th>
								<th align="center">도착시간</th>
							</tr>
							<tr>
								<td align="center"><input type="text" id="time_str" name="time_str"></td>
								<td align="center"><input type="text" id="arrival_time_str" name="arrival_time_str"></td>
							</tr>
						</table>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<button type="button" id="btn" class="btn btn-primary">삽입</button>
					</div>
				</form>
			</div>
			<!-- /.box -->
		</div>
		<!--/.col (left) -->

	</div>
	<!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<%@include file="../../../include/footer.jsp"%>
