<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@include file="../../../include/header.jsp"%>
 
<!-- 컨텐츠  Wrapper. -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  <h1>방학시간표 (2캠->지행)
	  	    <small>시간표</small>
	  </h1>
	  <ol class="breadcrumb">
	    <li><a href="/main"><i class="fa fa-dashboard"></i> 메인화면</a></li>
	    <li><a href="#">버스 시간표 관리</a></li>
        <li class="active">방학시간표 (2캠->지행)</li>
	  </ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-12">
				<div class="box-header">
					<div class="pull-right">
						<button type="button" class="btn btn-primary" onclick="location.href='/board/bus/vacation/c2jiInsert'">삽입</button>
						<button type="button" class="btn btn-primary" onclick="location.href='/board/bus/vacation/c2jiModify'">수정</button>
					</div>
				</div>
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">시간표</h3>
					</div>
					<div class="box-body">
						<table class="table table-bordered">
							<tr>
								<th style="width: 30">번호</th>
								<th>출발시간</th>
								<th>도착시간</th>
							</tr>
							<c:forEach items="${busList}" var="bus">
								<tr>
									<td>${bus.idx}</td>
									<td>${bus.time_str}</td>
									<td>${bus.arrival_time_str}</td>
								</tr>
							</c:forEach>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
			</div>
			<!--/.col (left) -->
	
		</div>
		<!-- /.row -->
	</section>
<!-- /.content -->
</div>
<script>
	var code = '${code}';
	
	if (code == '101') {
		alert("추가 되었습니다.");
	} else if (code == '102'){
		alert("수정 되었습니다.");
	} else if (code == '103'){
		alert("삭제 되었습니다.");
	}
</script>

<%@include file="../../../include/footer.jsp"%>
