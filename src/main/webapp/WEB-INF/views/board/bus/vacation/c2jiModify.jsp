<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@include file="../../../include/header.jsp"%>

 
<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/3.0.1/handlebars.js"></script>
<script>
$(document).ready(function(){
	
	function validateTime(time) {
		const re = /[0-2][0-9]:[0-5][0-9]/;
		return re.test(time);
	}
	  
	$('.modify').click(function(){ 
		
		var idx, time_str, arrival_time_str;
	    var url="/board/bus/vacation/c2jiModify";  
		
	    $(this).closest('tr').find('input').each(function(){
	        if($(this).attr("class") == "idx") idx = $(this).val();
	        if($(this).attr("class") == "time_str") time_str = $(this).val();
	        if($(this).attr("class") == "arrival_time_str") arrival_time_str = $(this).val();
	    });
	    
	    var datas = {"idx":idx, "time_str":time_str, "arrival_time_str":arrival_time_str};
	    console.log("datas"+datas);
	    
	    if(validateTime(time_str)&&validateTime(arrival_time_str)){
		    $.ajax({      
		        type: "POST",  
		        url: url,      
		        data: datas,      
		        success:function(args){
		        	location.href = "/board/bus/vacation/c2jiModify"
		        },   
		        error:function(e){  
		            alert(e.responseText);  
		        }  
		    });
	    } else {
        	alert("값이 올바르지 않습니다.");
	    }
	});
	    
	$('.btn-danger').click(function(){  
		
		var idx;
	    var url="/board/bus/vacation/c2jiDelete";  

	    $(this).closest('tr').find('input').each(function(){
	        if($(this).attr("class") == "idx") idx = $(this).val();
	    });
		
	    var datas = {"idx":idx};
	    
	    $.ajax({      
	        type: "POST",  
	        url: url,
	        data: datas,      
	        success:function(args){
	        	location.href = "/board/bus/vacation/c2jiModify"
	        },   
	        error:function(e){  
	            alert(e.responseText);  
	        }  
	    });
	});
});

</script>
<!-- 컨텐츠  Wrapper. -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  <h1>방학시간표 (2캠->지행)
	    <small>시간표</small>
	  </h1>
	  <ol class="breadcrumb">
	    <li><a href="/main"><i class="fa fa-dashboard"></i> 메인화면</a></li>
	    <li><a href="#">버스 시간표 관리</a></li>
        <li class="active">방학시간표 (2캠->지행)</li>
        <li class="active">수정</li>
	  </ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">시간표</h3>
						<div class="right">
							<button type="button" class="btn btn-primary pull-right" onclick="location.href='/board/bus/vacation/c2jiList'">리스트로 가기</button>
						</div>
					</div>
					<div class="box-body">
						<table class="table table-bordered">
							<tr>
								<td style="width: 30" align="center">번호</td>
								<td align="center">출발시간</td>
								<td align="center">도착시간</td>
								<td align="center">수정</td>
								<td align="center">삭제</td>
							</tr>
								<c:forEach items="${busList}" var="bus">
								<tr class ="tr">
									<td align="center"><input type="hidden" class="idx" value="${bus.idx}" readonly>${bus.idx}</td>
									<td align="center"><input type="text" class="time_str" value="${bus.time_str}"></td>
									<td align="center"><input type="text" class="arrival_time_str" value="${bus.arrival_time_str}"></td>
									<td align="center"><button type="button" class="btn btn-primary modify" value="${bus.idx}">수정</button></td>
									<td align="center"><button type="button" class="btn btn-danger" value="${bus.idx}">삭제</button>	</td>
								</tr>
								</c:forEach>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
			</div>
			<!--/.col (left) -->
	
		</div>
		<!-- /.row -->
	</section>
<!-- /.content -->
</div>

<%@include file="../../../include/footer.jsp"%>