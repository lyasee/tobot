<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page session="false"%>

<%@include file="../include/header.jsp"%>
 
<!-- 컨텐츠  Wrapper. -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  <h1>공지사항 게시판
	    <small>notice</small>
	  </h1>
	  <ol class="breadcrumb">
	    <li><a href="/main"><i class="fa fa-dashboard"></i> 메인화면</a></li>
	    <li><a href="#">사이트 관리</a></li>
        <li class="active">공지사항 게시판</li>
	  </ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">LIST PAGING</h3>
					</div>
					<div class="box-body">
						<table class="table table-bordered">
							<tr>
								<th style="width: 10%">번호</th>
								<th>제목</th>
								<th>작성자</th>
								<th>게시일</th>
								<th style="width: 10%x">뷰</th>
							</tr>
							<c:forEach items="${list}" var="boardVO">
								<tr>
									<td>${boardVO.idx}</td>
									<td><a href='/board/readPage?idx=${boardVO.idx}'>${boardVO.title} <strong></strong> </a></td>
									<td>${boardVO.writer}</td>
									<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${boardVO.regdate}"/></td>
									<td><span class="badge bg-red">${boardVO.viewcnt }</span></td>
								</tr>
							</c:forEach>
						</table>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
	
						<div class="text-center">
							<ul class="pagination">
	
								<c:if test="${pageMaker.prev}">
									<li><a href="notice?page=${pageMaker.startPage - 1}">&laquo;</a></li>
								</c:if>
	
								<c:forEach begin="${pageMaker.startPage }"
									end="${pageMaker.endPage }" var="idx">
									<li	<c:out value="${pageMaker.cri.page == idx?'class =active':''}"/>>
										<a href="notice?page=${idx}">${idx}</a>
									</li>
								</c:forEach>
	
								<c:if test="${pageMaker.next && pageMaker.endPage > 0}">
									<li><a href="notice?page=${pageMaker.endPage + 1}">&raquo;</a></li>
								</c:if>
	
							</ul>
						</div>
	
						<div>
							<button type="submit" class="btn btn-primary" onclick="location.href='/board/writePage'">글 작성</button>
						</div>
					</div>
					<!-- /.box-footer-->
				</div>
			</div>
			<!--/.col (left) -->
	
		</div>
		<!-- /.row -->
	</section>
<!-- /.content -->
</div>

<%@include file="../include/footer.jsp"%>
