<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>


<%@include file="../include/header.jsp"%>




<!-- 컨텐츠  Wrapper. -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>공지사항 작성
			<small>또봇 인환아 진학사 입사 축하한다.</small>
		</h1>
		<ol class="breadcrumb">
	    <li><a href="/main"><i class="fa fa-dashboard"></i> 메인화면</a></li>
	    <li><a href="#">사이트 관리</a></li>
        <li class="active">공지사항</li>
        <li class="active">공지사항 작성</li>
		</ol>
	</section>
 <!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">공지글 작성</h3>
				</div>
				<!-- /.box-header -->

				<form role="form" method="post" action="/board/writePage">
					<div class="box-body">
						<div class="form-group">
							<label for="exampleInputEmail1">제목</label> 
							<input type="text" name='title' class="form-control" placeholder="제목을 입력하세요...">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">본문</label>
							<textarea class="form-control" name="content" rows="3" placeholder="입력하세요 ..."></textarea>
						</div>
						<div class="form-group">
						<label for="exampleInputEmail1">작성자</label>
							<input type="text" name="writer" class="form-control" value="${login.nickname }" readonly >
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary">글 작성</button>
					</div>
				</form>
			</div>
			<!-- /.box -->
		</div>
		<!--/.col (left) -->

	</div>
	<!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<%@include file="../include/footer.jsp"%>