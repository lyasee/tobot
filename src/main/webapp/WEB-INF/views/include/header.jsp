<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
   
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>또봇 | 관리페이지</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="/resources/bower_components/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="/resources/bower_components/Ionicons/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="/resources/dist/css/AdminLTE.min.css">
	<!-- AdminLTE Skins. -->
	<link rel="stylesheet" href="/resources/dist/css/skins/_all-skins.min.css">
	<!-- Google Font -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	
</head>

<!-- ADD THE CLASS layout-boxed TO GET A BOXED LAYOUT  -->
<!-- 바디 태그 시작  -->
<body class="hold-transition skin-blue layout-boxed sidebar-mini">

	<!-- 사이트 wrapper -->
	<div class="wrapper">
		<header class="main-header">
			<!-- 로고 -->
			<a href="/main" class="logo">
				<!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini"><b>A</b>LT</span>
				<!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><b>또봇</b>Project</span>
			</a>
			
			<!-- 헤더 네비: style can be found in header.less -->
			<nav class="navbar navbar-static-top">
			
				<!-- 왼쪽 사이드바 토글 버튼-->
				<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
					<span class="sr-only">Toggle navigation</span>
				</a>
			
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<!-- 메세지 아이콘 드랍다운 -->
						<!-- <li class="dropdown messages-menu">
						  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
						    <i class="fa fa-envelope-o"></i>
						    <span class="label label-success">4</span>
						  </a>
						  <ul class="dropdown-menu">
						    <li class="header">You have 4 messages</li>
						    <li>
						      inner menu: contains the actual data
						      <ul class="menu">
						        <li>start message
						          <a href="#">
						            <div class="pull-left">
						              <img src="/resources/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
						            </div>
						            <h4>
						              Support Team
						              <small><i class="fa fa-clock-o"></i> 5 mins</small>
						            </h4>
						            <p>Why not buy a new awesome theme?</p>
						          </a>
						        </li>
						        end message
						      </ul>
						    </li>
						    <li class="footer"><a href="#">See All Messages</a></li>
						  </ul>
						</li> -->
						<!-- 알람 아이콘 드랍다운 -->
						<!-- <li class="dropdown notifications-menu">
						  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
						    <i class="fa fa-bell-o"></i>
						    <span class="label label-warning">10</span>
						  </a>
						  <ul class="dropdown-menu">
						    <li class="header">You have 10 notifications</li>
						    <li>
						      inner menu: contains the actual data
						      <ul class="menu">
						        <li>
						          <a href="#">
						            <i class="fa fa-users text-aqua"></i> 5 new members joined today
						          </a>
						        </li>
						      </ul>
						    </li>
						    <li class="footer"><a href="#">View all</a></li>
						  </ul>
						</li>
						Tasks: style can be found in dropdown.less
						<li class="dropdown tasks-menu">
						  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
						    <i class="fa fa-flag-o"></i>
						    <span class="label label-danger">9</span>
						  </a>
						  <ul class="dropdown-menu">
						    <li class="header">You have 9 tasks</li>
						    <li>
						      inner menu: contains the actual data
						      <ul class="menu">
						        <li>Task item
						          <a href="#">
						            <h3>
						              Design some buttons
						              <small class="pull-right">20%</small>
						            </h3>
						            <div class="progress xs">
						              <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
						                <span class="sr-only">20% Complete</span>
						              </div>
						            </div>
						          </a>
						        </li>
						        end task item
						      </ul>
						    </li>
						    <li class="footer">
						      <a href="#">View all tasks</a>
						    </li>
						  </ul>
						</li> -->
						<!-- 유저 정보 -->
						<li class="dropdown user user-menu">
						  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
						    <img src="/resources/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
						    <span class="hidden-xs">${login.nickname }</span>
						  </a>
						  <ul class="dropdown-menu">
						    <!-- User image -->
						    <li class="user-header">
						      <img src="/resources/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
						
						      <p>
						        	루지루지 - 웹 개발자
						        <small>Birth Day Mar. 1993</small>
						      </p>
						    </li>
						    <!-- Menu Body -->
						    <li class="user-body">
						      <div class="row">
						        <div class="col-xs-4 text-center">
						          <a href="#">Followers</a>
						        </div>
						        <div class="col-xs-4 text-center">
						          <a href="#">Sales</a>
						        </div>
						        <div class="col-xs-4 text-center">
						          <a href="#">Friends</a>
						        </div>
						      </div>
						    </li>
						    <!-- Menu Footer-->
						    <li class="user-footer">
						      <div class="pull-left">
						        <a href="#" class="btn btn-default btn-flat">프로필</a>
						      </div>
						      <div class="pull-right">
						        <a href="/logout" class="btn btn-default btn-flat">로그아웃</a>
						      </div>
						    </li>
						  </ul>
						</li>
					 	<!-- Control Sidebar Toggle Button -->
						<li>
						  <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
						</li>
					</ul>
				</div>
			</nav>
		</header>
		<!-- =============================================== -->
	
		<!-- 왼쪽 슬라이더바 aside -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- 슬라이더바 유저 패널 -->
				<div class="user-panel">
				  <div class="pull-left image">
				    <img src="/resources/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
				  </div>
				  <div class="pull-left info">
				    <p>${login.nickname }님 환영합니다.</p>
				    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
				  </div>
				</div>
				
				<!-- 검색 폼 -->
				<form action="#" method="get" class="sidebar-form">
				  <div class="input-group">
				    <input type="text" name="q" class="form-control" placeholder="Search...">
				        <span class="input-group-btn">
				          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
				          </button>
				        </span>
				  </div>
				</form>
		      
		      	<!-- 슬라이더 바 -->
				<ul class="sidebar-menu" data-widget="tree">
				  <li class="header">메인 매뉴</li>
				  <li class="treeview">
				    <a href="#">
				      <i class="fa fa-table"></i> <span>버스 시간표 관리</span>
				      <span class="pull-right-container">
				        <i class="fa fa-angle-left pull-right"></i>
				      </span>
				    </a>
				    <ul class="treeview-menu">
				      <li><a href="/board/bus/c2ji/c2jiList"><i class="fa fa-circle-o"></i> 2캠 -> 지행</a></li>
				      <li><a href="/board/bus/jic2/jic2List"><i class="fa fa-circle-o"></i> 지행 -> 2캠</a></li>
				      <li><a href="/board/bus/c1c2/c1c2List"><i class="fa fa-circle-o"></i> 1캠 순환 </a></li>
				      <li><a href="/board/bus/c2c1/c2c1List"><i class="fa fa-circle-o"></i> 2캠 순환 </a></li>
				      <li><a href="/board/bus/c2il/c2ilList"><i class="fa fa-circle-o"></i> 일산 통학버스(2캠) </a></li>
				      <li><a href="/board/bus/ilc2/ilc2List"><i class="fa fa-circle-o"></i> 일산 통학버스(일산) </a></li>
				      <li><a href="/board/bus/vacation/c2jiList"><i class="fa fa-circle-o"></i> 방학 시간표(2캠 -> 지행) </a></li>
				      <li><a href="/board/bus/vacation/jic2List"><i class="fa fa-circle-o"></i> 방학 시간표(지행 -> 2캠) </a></li>
				    </ul>
				  </li>
				  <li class="treeview">
				    <a href="#">
				      <i class="fa fa-table"></i> <span>식단 관리</span>
				      <span class="pull-right-container">
				        <i class="fa fa-angle-left pull-right"></i>
				      </span>
				    </a>
				    <ul class="treeview-menu">
				      <li><a href="#"><i class="fa fa-circle-o"></i> 식단 관리</a></li>
				    </ul>
				  </li>
				  <li class="treeview">
				    <a href="#">
				      <i class="fa fa-table"></i> <span>사이트 관리</span>
				      <span class="pull-right-container">
				        <i class="fa fa-angle-left pull-right"></i>
				      </span>
				    </a>
				    <ul class="treeview-menu">
				      <li><a href="#"><i class="fa fa-circle-o"></i>회원 추가</a></li>
				      <li><a href="/board/notice"><i class="fa fa-circle-o"></i>공지사항 게시판</a></li>
				    </ul>
				  </li>
				</ul>
			</section>
		</aside>
