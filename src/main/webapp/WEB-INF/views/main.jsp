<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@include file="include/header.jsp"%>

<!-- 컨텐츠  Wrapper. -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  <h1>메인화면
	  </h1>
	  <ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> 메인화면</a></li>
<!--    <li><a href="#">Layout</a></li>
        <li class="active">Boxed</li> -->
	  </ol>
	</section>

    <!-- Main content -->
    <section class="content">
		<div class="callout callout-info">
			<h4>또봇 런칭 !</h4>
			<p>최고의 서비스 제공을 목표로 합시다</p>
		</div>
        <div class="callout callout-success">
			<h4>시간표 추가 작업중</h4>
			<p>방학 시간표를 제외한 버스 시간표 입력 완료</p>
		</div>
<!--       Default box
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">제목</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
         	당신의 으마으마한 애플리케이션을 생성해보세요!
        </div>
        /.box-body
        <div class="box-footer">
          	발바닥
        </div>
        /.box-footer
      </div> -->
      <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.컨텐츠-wrapper -->

  
<%@include file="include/footer.jsp"%>