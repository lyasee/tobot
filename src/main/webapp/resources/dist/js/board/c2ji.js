/**

 * 
 */
$(document).ready(function(){
	
	function validateTime(time) {
		const re = /[0-2][0-9]:[0-5][0-9]/;
		return re.test(time);
	}
	  
	$('#modifyButton').click(function(){ 
		
		var idx, time_str, arrival_time_str;
	    var url="/board/bus/c2ji/c2jiModify";  
		
	    $(this).closest('tr').find('input').each(function(){
	        if($(this).attr("class") == "idx") idx = $(this).val();
	        if($(this).attr("class") == "time_str") time_str = $(this).val();
	        if($(this).attr("class") == "arrival_time_str") arrival_time_str = $(this).val();
	    });
	    
	    var datas = {"idx":idx, "time_str":time_str, "arrival_time_str":arrival_time_str};
	    console.log("datas"+datas);
	    
	    if(validateTime(time_str)&&validateTime(arrival_time_str)){
		    $.ajax({      
		        type: "POST",  
		        url: url,      
		        data: datas,      
		        success:function(args){
		        	location.href = "/board/bus/c2ji/c2jiModify"
		        },   
		        error:function(e){  
		            alert(e.responseText);  
		        }  
		    });
	    } else {
        	alert("값이 올바르지 않습니다.");
	    }
	});
	    
	$('.btn-danger').click(function(){  
		
		var idx;
	    var url="/board/bus/c2ji/c2jiDelete";  

	    $(this).closest('tr').find('input').each(function(){
	        if($(this).attr("class") == "idx") idx = $(this).val();
	    });
		
	    var datas = {"idx":idx};
	    
	    $.ajax({      
	        type: "POST",  
	        url: url,
	        data: datas,      
	        success:function(args){
	        	location.href = "/board/bus/c2ji/c2jiModify"
	        },   
	        error:function(e){  
	            alert(e.responseText);  
	        }  
	    });
	});
});